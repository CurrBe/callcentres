from gurobipy import *
import numpy as np
from math import floor, ceil
import msvcrt
import sys
import time
import os
import random

from data import read_data, gen_schedules, DATAPATH
from process_output import construct_sched, save_sched, process_cut_cache
from Benders_cuts import process_benders, most_violated_benders, \
    scaled_violation_benders, benders_multi_cut, most_violated_multi_benders
from MIR_cuts import MIR_benders, process_scen_MIR, MIR_scen, aggregate_cuts, multi_cut_MIR
from UB_heuristic import ub_mip

MIR_thresh = 10 ** -5


def base_mip(filename, alpha, num_scen=500, logging=True, time_lim = 900, 
    results_dir = "Results", std_log = "", seed = 42, sched_base_name = "Sched-alpha-",
    filepath = os.path.join(DATAPATH, "..", "Results", "ActualResults"),
    cap_sched_cost = -1):
    """
    Replicate the SAAP model used in the paper

    Parameters
    ----------
    `filename` : str
        Name of the data file

    `alpha` : fioat
        Value of the weight coefficient
    
    `num_scen` : int, optional
        Number of scenarios to calculate in the SIP, by default 500

    `logging` : bool, optional
        Whether to display Gurobi Logging, by default True

    `time_lim` : int, optional
        Maximum solve time for SIP model, by default 900

    `results_dir` : str, optional
        Directory where results will be stored, by default "Results"

    `std_log` : str, optional
        Name of the file prefix where logging is stored, by default ""

    `seed` : int, optional
        Seed to supply Gurobi and random number generator, by default 42

    `sched_base_name` : str, optional
        Prefix for the file name where results are stored, by default ""
        
    `filepath` : str, optional
        Path where the results will be saved, by default 
        "DATAPATH/../Results/ActualResults"

    Returns
    -------
    `XV` : dict
        Solved values for X

    `YV` : dict 
        Solved values for Y

    `I` : list
        Set of server classes

    `J` : list
        Set of customer classes

    `S` : list
        Set of shifts

    `T` : list
        Set of time periods

    `c` : dict
        Dictionary mapping server class and shift to a shift cost

    `data_time` : float
        Time taken to load model data

    `model_time` : float
        Time taken to solve the model
        
    `cut_cache` : list
        List of cuts made by the model of the form (cut, t value, type)
    """    

    filename = "AprilData.xlsx"
    # results_path = os.path.join(DATAPATH, "..", results_dir)
    results_path = filepath
    if not os.path.isdir(results_path):
        os.mkdir(results_path)

    np.random.seed(seed = seed)

    data_start = time.time()

    I, J, T, r, sigma, delta, mu, server_chain = read_data(filename)
    S, SP, a = gen_schedules(I, T)

    # cost coefficient for agent
    kappa = 0.1
    # penalty for losing customer j
    p = [1] * len(J)
    # overtime cost
    otc = 1.5
    # set of scenarios
    K = range(num_scen)

    R = np.zeros([len(J), len(K)])
    Gamma = np.zeros([len(J), len(T), len(K)])

    lam = np.zeros([len(J), len(T), len(K)])

    for k in K:
        for j in J:
            R[:, k] = np.random.multivariate_normal(r, sigma)
            for t in T:
                Gamma[j][t][k] = (R[j][k]** 2 * delta[j][t])
                lam[j][t][k] = np.random.poisson(Gamma[j][t][k])

    data_end = time.time()

    # shift cost per server type
    c = {(i, s): (1 + kappa) * min(16, len(s)) + otc * max(len(s) - 16, 0)
        for i in I for s in S}

    BMP = Model("BMP")

    X = {(i, s): BMP.addVar(vtype = GRB.INTEGER) for i in I for s in S}
    Y = {(i, t): BMP.addVar(vtype = GRB.INTEGER) for i in I for t in T}
    Z = {t: BMP.addVar() for t in T}

    # variables for Jensen's ineqality initialisation
    VB = {(i, j, t): BMP.addVar() for i in I for j in J for t in T}
    WB = {(j, t): BMP.addVar() for j in J for t in T}

    BMP.setObjective(quicksum(c[i, s] * X[i, s] for i in I for s in S) + \
        alpha * quicksum(Z[t] for t in T), GRB.MINIMIZE)

    SchedLB = {(i, t): BMP.addConstr(
        quicksum(a[i, s, t] * X[i, s] for s in S) >= Y[i, t]
    )
    for i in I for t in T}

    NumServersUB = {i: BMP.addConstr(
        quicksum(X[i, s] for s in S) <= 50
    )
    for i in I}

    PTUB = BMP.addConstr(quicksum(X[i, s] for i in I for s in SP) <= 4)

    if cap_sched_cost > 0:
        print("******** CAP COST: ", cap_sched_cost)
        BMP.addConstr(quicksum(c[i, s] * X[i, s] for i in I for s in S) <= cap_sched_cost)

    XLB = {(i, s): BMP.addConstr(X[i, s] >= 0) for i in I for s in S}
    YLB = {(i, t): BMP.addConstr(Y[i, t] >= 0) for i in I for t in T}
    ZLB = {t: BMP.addConstr(Z[t] >= 0) for t in T}

    BSP = Model("BSP")
    BSP.setParam("OutputFlag", 0)

    W = {j: BSP.addVar() for j in J}
    V = {(i, j): BSP.addVar() for i in I for j in J}

    BSP.setObjective(quicksum(p[j] * W[j] for j in J))

    ServeDemand = {j: 
        BSP.addConstr(quicksum(mu[i][j] * V[i, j] for i in I) + W[j] == 0)
    for j in J}

    ServeUB = {i: BSP.addConstr(quicksum(V[i, j] for j in J) <= 0)
    for i in I}

    VLB = {(i, j): BSP.addConstr(V[i, j] >= 0)
    for i in I for j in J}

    VLB = {j: BSP.addConstr(W[j] >= 0)
    for j in J for t in T}

    model_start = time.time()

    for v in X.values():
        v.vtype = "C"

    for v in Y.values():
        v.vtype = "C"

    BMP.setParam("OutputFlag", 0)
    
    if std_log == "":
        log_filename = 'LogFilealpha%f.txt'%alpha

    if os.path.isfile(os.path.join(results_path, log_filename)):
        os.remove(os.path.join(results_path, log_filename))

    BMP.setParam('LogFile', os.path.join(results_path, log_filename))

    cut_cache = {t: [(tuple([0] * (len(I) + 1)), "Zero")] for t in T}

    multi_cut_cache = {(t, k): [tuple([0] * (len(I) + 1))] for t in T for k in K}

    while True:
        total_sub = 0

        Added_Benders = 0
        Added_MIR = 0
        Added_MIR_agg = 0

        BMP.optimize()

        YV = {k: Y[k].x for k in Y.keys()}
        ZV = {k: Z[k].x for k in Z.keys()}

        for t in T:
            gammas = {(j, k): 0 for j in J for k in K}
            pis = {(i, k): 0 for i in I for k in K}

            q = 0
            for k in K:
                for i in I:
                    ServeUB[i].RHS = Y[i, t].x
                for j in J:
                    ServeDemand[j].RHS = lam[j][t][k]
                BSP.optimize()
                
                Q = BSP.objVal
                q += Q/len(K)

                for j in J:
                    gammas[j, k] = ServeDemand[j].pi
                for i in I:
                    pis[i, k] = ServeUB[i].pi

            if q > Z[t].x + 0.0001:
                Added_Benders += 1
                d = process_benders(Y, I, J, K, t, gammas, pis, lam)
                BMP.addConstr(Z[t] >= d[0] - quicksum(d[i + 1] * Y[i, t] for i in I))
                
                cut_cache[t].append((d, t, "Benders Warm Start"))
            
            c_cut = most_violated_benders(ZV, YV, cut_cache[t], t, I)
            d1 = cut_cache[t][c_cut][0]
            new_MIR_cut, max_violation, most_violated_c = MIR_benders(ZV, YV, Z, Y, d1, c_cut, cut_cache[t], t, I)
            if len(new_MIR_cut) > 0:
                BMP.addConstr(Z[t] >= new_MIR_cut[0] - quicksum(new_MIR_cut[i + 1] * Y[i, t] for i in I))
                cut_cache[t].append((new_MIR_cut, t, "MIR Warm Start"))
                Added_MIR += 1

        print(("Benders", Added_Benders, "MIR", Added_MIR,  "Agg MIR", Added_MIR_agg, BMP.objVal))
        if Added_Benders == 0:
            break

    for v in X.values():
        v.vtype = "I"
    for v in X.values():
        v.vtype = "I"

    BMP._BestObj = GRB.INFINITY
    BMP._lastnode = -GRB.INFINITY
    BMP._X = X
    BMP._Y = Y
    BMP._Z = Z
    BMP._BestX = None
    BMP._BestY = None
    BMP._BestZ = None
    BMP._Lazy = 0
    BMP._user = 0

    BMP._I = I
    BMP._J = J
    BMP._K = K
    BMP._S = S
    BMP._SP = SP
    BMP._T = T

    BMP._BSP = BSP
    BMP._ServeUB = ServeUB
    BMP._ServeDemand = ServeDemand

    BMP._lam = lam
    BMP._mu = mu
    BMP._alpha = alpha
    BMP._c = c
    BMP._a = a
    BMP._p = p

    BMP._cut_cache = cut_cache

    BMP._ub_model = Model("UB")
    BMP._ub_sub = Model("Sub")

    BMP._ub_model.setParam("OutputFlag", 0)
    BMP._ub_sub.setParam("OutputFlag", 0)

    if logging:
        BMP.setParam("OutputFlag", 1)
    BMP.setParam("LazyConstraints", 1)
    BMP.setParam("MIPGAP", 0.005)
    BMP.setParam('BranchDir',1)
    BMP.setParam("PreCrush", 1)
    BMP.setParam("Method", 2)
    BMP.setParam("TimeLimit", time_lim)
    BMP.setParam("Seed", seed)

    BMP.optimize(Callback)

    model_end = time.time()

    data_time = data_end - data_start
    model_time = model_end - model_start

    # solved variables
    XV = {(i, s): X[i, s].x for (i, s) in X}
    YV = {(i, t): Y[i, t].x for i in I for t in T}

    abandonments = {k: 0 for k in K}
    arrivals = {k: sum(lam[j][t][k] for j in J for t in T) for k in K}
    abandonment_props = {k: 0 for k in K}
    for k in K:
        for t in T:
            for j in J:
                ServeDemand[j].RHS = lam[j][t][k]
            for i in I:
                ServeUB[i].RHS = YV[i, t]
            
            BSP.optimize()
            abandonments[k] += BSP.objVal
    
    try:
        abandonment_props = {k: (abandonments[k]/arrivals[k] if arrivals[k] > 0 else 0) for k in K}
        mean_abandonment = sum(abandonment_props[k] for k in K)/len(K)

        print("Abandonment prop base model: ", mean_abandonment)
    except Exception:
        print("We tried fam")

    sched, worker_types = construct_sched(XV, I, S, T)
    save_sched(sched, worker_types, filepath, "%s%f.txt"%(sched_base_name, alpha))

    return XV, YV, I, J, S, T, c, data_time, model_time, BMP._cut_cache

def Callback(model, where):
    if where == GRB.Callback.MIPSOL:
        node_count = model.cbGet(GRB.Callback.MIPSOL_NODCNT)
        XV = {k:v for (k, v) in zip(model._X.keys(), model.cbGetSolution(list(model._X.values())))}
        YV = {k:v for (k, v) in zip(model._Y.keys(), model.cbGetSolution(list(model._Y.values())))}
        ZV = {k:v for (k, v) in zip(model._Z.keys(), model.cbGetSolution(list(model._Z.values())))}

        CandObj = sum(model._c[i, s] * XV[i, s] for i in model._I for s in model._S)

        for t in model._T:
            q = 0
            gammas = {(j, k): 0 for j in model._J for k in model._K}
            pis = {(i, k): 0 for i in model._I for k in model._K}
            for k in model._K:
                for i in model._I:
                    model._ServeUB[i].RHS = YV[i, t]
                for j in model._J:
                    model._ServeDemand[j].RHS = model._lam[j][t][k]
                
                model._BSP.optimize()

                q += model._BSP.objVal/len(model._K)

                for j in model._J:
                    gammas[j, k] = model._ServeDemand[j].pi
                for i in model._I:
                    pis[i, k] = model._ServeUB[i].pi
            
            CandObj += model._alpha * q
            
            if q > ZV[t] + 0.0001:
                d = process_benders(model._Y, model._I, model._J, model._K, t, gammas, pis, model._lam)

                violation = min(d[0] + sum(d[i + 1] * YV[i, t] for i in model._I) - ZV[t], 0)
                violation = violation/np.linalg.norm(np.array([1] + list(d)))

                # if violation > 10 ** -5:
                model.cbLazy(model._Z[t] >= d[0] - quicksum(d[i + 1] * model._Y[i, t] for i in model._I))

                model._cut_cache[t].append((d, t, "Benders CB"))

                model._Lazy += 1
                if node_count % 100 == 0 or node_count < 5:
                    c_cut = most_violated_benders(ZV, YV, model._cut_cache[t], t, model._I)
                    d1 = model._cut_cache[t][c_cut][0]
                    new_MIR_cut, max_violation, most_violated_c = MIR_benders(ZV, YV, model._Z, model._Y, d1, c_cut, model._cut_cache[t], t, model._I)
                    if node_count == 0:
                        MIR_thresh = 10 ** -4
                    else:
                        MIR_thresh = 5 * 10 ** -4
                    if len(new_MIR_cut) == 0 or max_violation < MIR_thresh:
                        continue
                    model.cbLazy(model._Z[t] >= new_MIR_cut[0] - quicksum(new_MIR_cut[i + 1] * model._Y[i, t] for i in model._I))
                    model._cut_cache[t].append((new_MIR_cut, t, "MIR CB"))
                    model._user += 1
                ZV[t] = q

        if CandObj < model._BestObj:
            print("FOUND ONE: ", (CandObj, model._BestObj, model.cbGet(GRB.callback.MIPSOL_OBJBST)))
            model._BestObj = CandObj
            model._BestX = list(XV.values())
            model._BestY = list(YV.values())
            model._BestZ = list(ZV.values())

    if where == GRB.callback.MIPNODE and \
            model.cbGet(GRB.callback.MIPNODE_STATUS)==GRB.OPTIMAL and \
            model.cbGet(GRB.callback.MIPNODE_OBJBST) > model._BestObj+0.0001:    
        model.cbSetSolution(list(model._X.values()), model._BestX)
        model.cbSetSolution(list(model._Y.values()), model._BestY)
        model.cbSetSolution(list(model._Z.values()), model._BestZ)
        ob = model.cbUseSolution()
        print("**** ob = %f *****"%ob)
        if ob < model.cbGet(GRB.Callback.MIPNODE_OBJBST):
            model._BestObj = ob
            print('Posting', ob)

def main():
    filename = "AprilData.xlsx"
    alpha = 1.5
    XV, YV, I, J, S, T, c, data_time, model_time, cut_cache = base_mip(filename, alpha, num_scen=500, seed = random.randint(0, 2 ** 5))
    sched, worker_types = construct_sched(XV, YV, I, J, S, T)
    save_sched(sched, worker_types, os.path.join(DATAPATH, "..", "Results"), "Sched_test_alpha_%f.txt"%alpha)
    print("Sched: ")
    print(sched)

    cut_file = "cut_summary.txt"
    process_cut_cache(cut_cache, cut_file)

    print("Data Loading Time: %f"%data_time)
    print("Total Model Solve Time: %f"%model_time)

if __name__ == "__main__":
    main()