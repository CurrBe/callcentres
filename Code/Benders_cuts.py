from gurobipy import *

import numpy as np
from math import floor, ceil

def process_benders(Y, I, J, K, t, gammas, pis, lam):
    d0 = 1/len(K) * sum(gammas[j, k] * lam[j][t][k] for j in J for k in K)
    d = tuple([d0] + [-1/len(K) * sum(pis[i, k] for k in K) for i in I])

    return d
    # if lazy: # and scaled_violation_benders(ZV, YV, d, t, I) >= 10 ** -5:
    #     BMP.cbLazy(Z[t] >= 1/len(K) * quicksum(sum(gammas[j, k] * lam[j][t][k] for j in J) + \
    #         quicksum(pis[i, k] * Y[i, t] for i in I) for k in K))
    # else:
    #     BMP.addConstr(Z[t] >= 1/len(K) * quicksum(sum(gammas[j, k] * lam[j][t][k] for j in J) + \
    #         quicksum(pis[i, k] * Y[i, t] for i in I) for k in K))
    # return BMP, d

def most_violated_benders(ZV, YV, cut_cache, t, I):
    max_violated = -np.inf
    max_c = -1
    benders_cuts = [c for c in cut_cache if c[-1] == "Benders"]
    for c in range(len(benders_cuts)):
        d = benders_cuts[c][0]
        violation = max(0, d[0] - sum(d[i + 1] * YV[i, t] for i in I) - ZV[t], 0)
        violation = violation/np.linalg.norm(np.array([1] + list(d)))
        if violation > max_violated:
            max_violated = violation
            max_c = c
    return max_c

def benders_multi_cut(YV, I, J, k, t, gammas, pis, lam, lazy=False):
    d0 = sum(lam[j][t][k] * gammas[j, k] for j in J)
    d = tuple([d0] + [-pis[i, k] for i in I])
    return d

def most_violated_multi_benders(YV, Q, multi_cut_cache, t, k, I):
    max_violated = -np.Inf
    max_c = -1
    cut_list = multi_cut_cache[t, k]

    for c in range(len(cut_list)):
        d = cut_list[c]
        violation = max(d[0] - sum(d[i] * YV[i, t] for i in I) - Q, 0)
        violation = violation/np.linalg.norm(np.array([1] + list(d)))
        
        if violation > max_violated:
            max_violated = violation
            max_c = c
    return max_c, max_violated

def scaled_violation_benders(ZV, YV, d, t, I):
    # print({k: Y[k].x for k in Y.keys()})
    # print({k: Z[k].x for k in Z.keys()})
    violation = max(d[0] + sum(d[i + 1] * YV[i, t] for i in I) - ZV[t], 0)
    scaled_v = violation/np.linalg.norm(np.array([1] + list(d)))
    return scaled_v