import numpy as np
import math
import random
from tqdm import *
import os
import msvcrt
import time

from process_output import construct_sched, save_sched, read_sched
from data import DATAPATH, read_data
from misc import pad_array

class Simulation:
    """
    Class that oversees the running of the  SHADOW-TANDEM simulation

    ...

    Attributes
    ----------
    clock : float
        time as a fraction of a 30 minute period, keeps track of progress in a
        period
    
    period : int
        number of the current period

    complete : bool
        True if the simulation is complete

    abandonments : int
        current number of customers who have abandoned the queue

    served : int
        current number of customers who have been served

    tot_arrivals : int
        current number of customer arrivals

    arrival_types : list
        current number of arrivals of each customer type

    abandonment_percentage : float
        percentage of customers who have abandoned so far

    I : list
        set of server types

    J : list
        set of customer types

    T : list
        set of time periods

    mu : array-like
        matrix where element (i, j) is the service rate for server type i
        processing customer type j

    R_mean : array-like
        mean number of arrivals per day for each customer class

    sigma : array-like
        covariance of the arrivals per day for each customer class

    delta : array-like
        expected proportion of customer arrivals for type j customer in each
        period

    r : float 
        constant for SHADOW-TANDEM 

    tau : float
        constant for SHADOW-TANDEM 

    eta : float
        constant for SHADOW-TANDEM 

    c : float
        constant for SHADOW-TANDEM 

    sched : array-like
        defined schedule of workers over T

    worker_types : list
        server class for each worker rostered

    arrival_times : list
        next arrival time for each customer class, infinity if no arrival

    update_time : float
        a time comstant at which an amount of traffic is removed from the queue,
        specified by SHADOW-TANDEM

    E : list
        set of activities (edges (i, j) on a graph indicating that server class 
        i can serve customer class j)

    act_map : dict
        dictionary mapping each customer class to the list of servers that can
        process it

    queues : list
        list of queues being handled by the simulator

    serve_ass : list
        list of the number of servers scheduled for each server class

    Methods
    -------
    init_queus(t):
        create new queue states in period t

    adv_time():
        move forward to the nezt time event

    sample_arrival_rate():
        randomly sample customer arrival rates for the planning horizon

    gen_next_arrival_time(j):
        generate the next arrival time for customer class j

    generate_arrival(j):
        generate a customer object to enter the simulation

    process_arrival(cust):
        process a customer and assign them to an available queue

    adv_period():
        move forward by one period

    """    
    def __init__(self, filepath, filename, I, J, T, mu, R_mean, sigma, delta, 
            Gamma, r, tau, eta, c, sched = None, worker_types = None, 
            init_num_abandonments = 0, init_num_arrivals = 0, start_arrivals = None, start_queues = None):
        """
        Parameters
        ----------
        filename : str
            Name of the file containing desired scheduke

        I : list
            set of server types

        J : list
            set of customer types

        T : list
            set of time periods

        mu : array-like
            matrix where entry (i, j) is rate of service for server class i
            and customer class j

        R_mean : array-like
            mean vector of daily arrivals for each customer class

        sigma : array-like
            covariance matrix for daily arrivals for each customer class
            
        delta : array-like
            time distribution vector for arrivals per period for each customer
            class

        r : float
            SHADOW-TANDEM constant

        tau : float
            SHADOW-TANDEM constant

        eta : float
            SHADOW-TANDEM constant

        c : float
            SHADOW-TANDEM constant
        """        

        self.clock = 0
        self.period = 0

        self.complete = False

        self.abandonments = init_num_abandonments
        self.served = 0
        self.tot_arrivals = init_num_arrivals
        self.arrival_types = [0] * len(J)
        self.abandonment_percentage = 0
        self.abandonment_profile = [0] * len(T)
        self.queue_states = {(i, t): [[], []] for i in I for t in T}
        self.start_queues = start_queues
        self.arrivals_period = [0] * len(T)
        
        self.I = I
        self.J = J
        self.T = T
        self.mu = mu
        self.R_mean = R_mean
        self.sigma = sigma
        self.delta = delta
        self.r = r
        self.tau = tau
        self.eta = eta
        self.c = c

        if sched is None or worker_types is None:
            sched, worker_types = read_sched(filepath, filename, T)

        self.sched = sched
        self.worker_types = worker_types
        self.num_workers = self.sched.shape[0]
        self.worker_status = [0] * self.num_workers

        self.arrival_index = {t: [np.Inf for _ in J] for t in T}

        # self.sample_arrival_rate()
        self.Gamma = Gamma
        
        self.arrival_times = [np.Inf] * len(self.J)
        
        self.update_time = self.tau/self.r * 100

        E = []
        for i in self.I:
            for j in self.J:
                if mu[i][j] > 0:
                    E.append((j, i))
        self.E = E
        self.act_map = {j: [i for i in self.I if (j, i) in self.E] for j in self.J}

        self.init_queues()

        for j in self.J:
            self.gen_next_arrival_time(j)

        if start_arrivals is not None:
            for j in J:
                if start_arrivals[j] < np.Inf:
                    self.arrival_times[j] = start_arrivals[j]

        self.arrival_index[0] = self.arrival_times

    def init_queues(self):
        """
        Initialise queues for new period of the simultation
        """   

        if self.period > 0:
            for i in self.I:
                self.queue_states[i, self.period - 1] = [self.queues[i].high_queue, self.queues[i].low_queue]

        self.queues = []
        self.serve_ass = [0] * len(self.I)
        for n in range(self.num_workers):
            self.worker_status[n] = self.sched[n, self.period]
            if self.worker_status[n]:
                self.serve_ass[self.worker_types[n]] += 1

        for i in self.I:
            self.queues.append(Queue(i, self.serve_ass[i], list(self.mu[i, :])))
            if self.start_queues is not None and self.period == 0:
                if len(self.start_queues[i][0]) > 0:
                    self.queues[i].high_queue = self.queue_states[i, 0][0] = self.start_queues[i][0]
                if len(self.start_queues[i][1]) > 0:
                    self.queues[i].low_queue = self.queue_states[i, 0][1] = self.start_queues[i][1]
                     

        self.service_times = pad_array(np.array([[np.Inf for n in range(self.serve_ass[i])] for i in self.I], dtype = object))

    def adv_time(self):
        """
        Advance a time step in the simulation and process relevant events
        """      

        if self.eta * sum(self.queues[i].QH + self.queues[i].QL for i in self.I) < 1:
            for i in self.I:
                self.queues[i].QH = max(self.queues[i].QH - self.c, 0)
                self.queues[i].QL = max(self.queues[i].QL - self.c, 0)

        next_arrival = min(self.arrival_times)

        serve_times_array = np.array(self.service_times)

        old_serve_times_array = serve_times_array

        # print(serve_times_array)

        try:
            next_service = np.min(serve_times_array)
        except Exception:
            # handle case where there are no workers assigned at all
            next_service = np.Inf
        
        next_time_step = min(next_arrival, next_service, self.update_time)

        self.clock = next_time_step

        if next_arrival < next_service and next_arrival < self.update_time:
            arrival_ind = self.arrival_times.index(next_arrival)
            self.generate_arrival(arrival_ind)
            self.tot_arrivals += 1
            self.arrival_types[arrival_ind] += 1
            # print(arrival_ind)
        elif next_service < next_arrival and next_service < self.update_time:
            service_ind = np.unravel_index(np.argmin(serve_times_array, axis=None), serve_times_array.shape)
            service_queue = self.queues[service_ind[0]]
            try:
                service_queue.status[service_ind[1]] = 0
                self.service_times[service_ind[0]][service_ind[1]] = np.Inf
                self.served += 1
            except Exception:
                print("STA: ", serve_times_array)
                print("Service Ind: ", service_ind)
                print("Queue status: ", service_queue.status)
                print("STA0: ", serve_times_array[service_ind[0]])
                print("Time period: ", self.period)
                raise Exception
        else:
            for i in self.I:
                self.queues[i].QH = max(self.queues[i].QH - self.tau, 0)
                self.queues[i].QL = max(self.queues[i].QL - self.tau, 0)

            self.update_time += self.tau/self.r

        for q in self.queues:
            for i in range(len(q.high_queue)):
                cust = q.high_queue[i]
                if self.clock >= cust.abandon_time:
                    self.abandonments += 1
                    self.abandonment_profile[self.period] += 1
                    q.high_queue.pop(i)
            for i in range(len(q.low_queue)):
                cust = q.low_queue[i]
                if self.clock >= cust.abandon_time:
                    self.abandonments += 1
                    self.abandonment_profile[self.period] += 1
                    q.low_queue.pop(i)

        if self.clock >= 1:
            self.adv_period()
    
    def sample_arrival_rate(self):
        """
        Randomly sample arrival rates for customers. These are the rates for the 
        Poisson arrival process.
        """     

        R = np.random.multivariate_normal(self.R_mean, self.sigma)
        self.Gamma = np.zeros([len(self.J), len(self.T)])
        for j in self.J:
            for t in self.T:
                self.Gamma[j][t] = (R[j]* self.delta[j][t]) ** 2 

    def gen_next_arrival_time(self, j):
        """
        Randomly generate the time of the next arrival for customer class j

        Parameters
        ----------
        j : int
            label for a customer class
        """        
        if self.Gamma[j][self.period] > 0:
            self.arrival_times[j] = self.clock + np.random.exponential(scale=1/self.Gamma[j][self.period])
            self.arrivals_period[self.period] += 1
        else:
            self.arrival_times[j] = np.Inf

    def generate_arrival(self, j):
        """
        Generate a new arrival for a customer class to the simulation

        Parameters
        ----------
        j : int
            lavbel for a customer class
        """        

        priorities = [0, 1, 2]
        priority = random.sample(priorities, 1)[0]
        cust = Customer(j, priority)
        self.process_arrival(cust)
        self.gen_next_arrival_time(j)

    def process_arrival(self, cust):
        """
        Assign a newly arrived customer to an appropriate queue according to 
        SHADOW-TANDEM routing rules

        Parameters
        ----------
        cust : Customer
            newly arrived customer
        """        
        if cust.priority == 2:
            Qs = [self.queues[i].QH for i in self.act_map[cust.type]]
        else:
            Qs = [self.queues[i].QL for i in self.act_map[cust.type]]
        
        volumes = [(self.eta * Qs[i] * self.mu[self.I[i], cust.type]/ \
            self.serve_ass[self.I[i]] if self.serve_ass[self.I[i]] > 0 else np.Inf) 
            for i in range(len(self.act_map[cust.type]))]
        # threshold that dictates whether to tag a customer arrival or not
        threshold = min(volumes)

        # if cust.type == 0:
        #     print("V: ", volumes)
        #     print("threshold = ", threshold)
        
        if cust.patience < threshold:
            cust.tagged = True

        # route customer to most ideal pool m
        idx = volumes.index(threshold)
        
        m = self.act_map[cust.type][idx]
        
        
        serve_ind, serve_time = self.queues[m].arrival(cust, self.clock)

        # if idx == 0:
        #     print("serve ind = ", serve_ind)
        #     print("serve time = ", serve_time)

        if serve_ind >= 0:
            # print("m = ", m)
            # print("SI = ", serve_ind)
            # print(self.queues[m].status)
            self.service_times[m][serve_ind] = self.clock + serve_time

    def adv_period(self):
        """
        Move the simulation forward by one period, reset all queues
        """        
        self.clock = 0
        self.period += 1
    
        if self.tot_arrivals > 0:
            self.abandonment_percentage = self.abandonments/self.tot_arrivals
            # self.abandonment_profile[self.period - 1] /= len(self.abandonment_profile[self.period - 1])

        if self.period < len(self.T):
            self.init_queues()
            for j in self.J:
                self.arrival_times[j] -= 1
        
            self.arrival_index[self.period] = self.arrival_times

            self.service_times -= 1

            for n in range(self.num_workers):
                # if finishing shift with a customer still being served
                if self.worker_status[n] and not self.sched[n, self.period]:
                    serve_type = self.worker_types[n]
                    idx = n - sum(self.serve_ass[ii] for ii in self.I[:serve_type])
                    if self.service_times[serve_type, idx] < np.Inf:
                        time_array = [0 if m == np.Inf else m for m in 
                            self.service_times[serve_type]]                        
                        most_available = np.min(time_array)
                        most_available_idx = np.argmin(time_array)[0]
                        if self.service_times[serve_type, most_available_idx] == np.Inf:
                            self.service_times[serve_type, most_available_idx] = self.service_times[serve_type, idx]
                        else:
                            self.service_times[serve_type, most_available] += self.service_times[serve_type, idx]

                        # if most_available_idx == 0:
                        #     print("SERVER SWAP TO 0", serve_type)
        else:
            # self.abandonment_profile = [i/self.tot_arrivals if self.tot_arrivals > 0 else 0 for i in self.abandonment_profile]
            self.complete = True
            self.final_queues = {}
            for i in self.I:
                if len(self.queues[i].high_queue) == 0 and len(self.queues[i].low_queue) == 0:
                    self.final_queues[i] = None
                else:
                    self.final_queues[i] = [self.queues[i].high_queue, self.queues[i].low_queue]
        
        

class Queue:
    """
    A Class representing a Queue, which implements the SHADOW-TANDEM Algorithm

    ...

    Attributes
    ----------
    serve_type : int
        integer label for type of server processing the queue

    num_servers : int
        number of servers maintaining the queue

    mu : arra-like
        vector where the element at index j is the expected time for the server
        to process customer type j

    high_queue : list
        queue for higher-priority customers

    low_queue : list
        queue for lower-priority customers

    QH : float
        Q-value for high_queue indicated in the SHADOW-TANDEM algorithm, 
        indicates relative traffic amount in the queue

    QL : float
        Q-value for low_queue indicated in the SHADOW-TANDEM algorithm, 
        indicates relative traffic amount in the queue

    status : list
        list where 0 in index n indicates server number n is unoccupied, 1 for
        occupied

    Methods
    -------
    arrival(cust, t):
        processes an arrival of a customer in the queue at time t by assigning
        an available server (if one exists) and calculating a service time

    service(server_ind, t):
        represents server at index server_ind serving a customer at the front of
        the queue at time t

    reset():
        resets the queue
    """    

    def __init__(self, serve_type, num_servers, mu):
        """
        Parameters
        ----------
        serve_type : int
            integer label for type of server maintaining the queue
        num_servers : int
            Number of servers maintaining the queue
        mu : array-like
            vector where the element at index j is the expected time for the 
            server to process customer type j
        """        
        self.high_queue = []
        self.QH = 0
        self.low_queue = []
        self.QL = 0
        self.type = serve_type
        self.mu = mu
        self.num_servers = num_servers
        # initially all servers available
        self.status = [0] * num_servers
    
    def arrival(self, cust, t):
        """
        Arrival of Customer in queue

        Parameters
        ----------
        cust : Customer
            customer arriving in the queue
        t : float
            time (as a fraction of a 30 min period) the customer arrives

        Returns
        -------
        server_ind : int
            index in list of servers that processes customer
        
        serve_time : float
            time the server takes to serve the customer
        """        

        if not cust.tagged:
            self.low_queue.append(cust)
        elif cust.priority == 2:
            self.high_queue.append(cust)
            if self.num_servers > 0:
                self.QH += self.mu[cust.type]/self.num_servers
            else:
                self.QH = np.Inf
        else:
            self.low_queue.append(cust)
            if self.num_servers > 0:
                self.QL += self.mu[cust.type]/self.num_servers
            else:
                self.QL = np.Inf
        
        serve_time = 0
        serve_ind = -1
        # if self.type == 0:
        #     print("NS: ", self.num_servers)
        #     print("STATUS: ", self.status)
        for i in range(self.num_servers):
            if self.status[i] == 0:
                serve_time = self.service(i, t)
                serve_ind = i
                return serve_ind, serve_time

        return serve_ind, serve_time
    
    def service(self, server_ind, t):
        """
        Serve customer at the front of the queue

        Parameters
        ----------
        server_ind : int
            index of serve that processes the next customer

        t : float
            time the service begins

        Returns
        -------
        serve_time : float
            time the service ends

        Raises
        ------
        Exception
            preoccupied server gets assigned to a customer
        """        

        if len(self.high_queue) > 0:
            queue = self.high_queue
        elif len(self.low_queue) > 0:
            queue = self.low_queue
        else:
            return np.Inf
        cust = queue.pop(0)
        serve_time = np.random.exponential(scale=1/self.mu[cust.type])

        cust.enter_queue(self.mu, t)

        if self.status[server_ind] == 1:
            raise Exception("Server %d Unavailable!"%server_ind)

        self.status[server_ind] = 1

        return serve_time

    def reset(self):
        """
        Resent the queue by removing all customers still in the queue, keep the
        customers that are currently being served
        """        
        self.QH = 0
        self.QL = 0
        self.high_queue = []
        self.low_queue = []
        self.num_servers = 0
        self.status = [0] * self.num_servers


class Customer:
    """
    A Classed used to represent a Customer

    ...

    Attributes
    ----------
    type : int
        value indicating the customer class
    
    priority : int
        value indicating priority (2 for high, 0 and 1 for low)
    
    patience : float
        time the customer will wait in queue before abandoning
    
    tagged : bool
        indicates whether the customer has been tagged by the SHADOW-RM algorithm

    abandon_time : float
        time at which the customer will leave the queue

    abandoned : bool
        indicates whether a customer has left the queue
    
    Methods
    -------
    calc_patience(mu):
        calculates the patience rate of the customer
    
    enter_queue(mu, t):
        signifies customer entering a queue, calculate the abandonment time
        relative to time t
    """    
    
    def __init__(self, cust_type, priority):
        """
        Parameters
        ----------
        cust_type : int
            integer label for customer class
        priority : int
            0 or 1 for low priority, 2 for high priority

        Raises
        ------
        ValueError
            invalid customer priority value entered
        """        

        self.type = cust_type
        if priority not in range(3):
            raise ValueError("Customer Priority must be 0, 1 or 2")
        self.priority = priority
        self.patience = 0
        self.tagged = False
        self.abandon_time = 0
        self.abandoned = False

    def __repr__(self):
        return "Customer(type: %d, priority: %d)"%(self.type, self.priority)

    def __str__(self):
        return "Customer(type: %d, priority: %d)"%(self.type, self.priority)

    def calc_patience(self, mu):
        """
        Calculate patience rate

        Parameters
        ----------
        mu : array_like
            vector of service times for each class of customer by a given server
            type
        """        

        rate = 0.95 * mu[self.type]
        self.patience = rate

    def enter_queue(self, mu, t):
        """
        Processes a customer entering a queue

        Parameters
        ----------
        mu : array-like
            vector of service times for each class of customer by a given server
            type
        t : float
            the current time, as a fraction of a 30 minute period
        """        

        self.calc_patience(mu)
        self.abandon_time = t + np.random.exponential(self.patience)


def main():
    # filename = "Full20Ktest.txt"
    # filename = "AllOnes.txt"
    # filename = "sched_base_alpha1.600000.txt"
    filepath = "Comparison"
    filename = "Sched-alpha-1.500000.txt"
    data_filename = "AprilData.xlsx"

    I, J, T, R_mean, sigma, delta, mu, server_chain = read_data(os.path.join(DATAPATH, data_filename))

    r = 100
    tau = 1
    eta = 0.01
    c = 2

    # print(R_mean)
    # print(sigma)

    K = range(1)

    Gamma = np.zeros([len(J), len(T), len(K)])
    for k in K:
        R = np.random.multivariate_normal(R_mean, sigma)
        for j in J:
            for t in T:
                Gamma[j][t][k] = (R[j] **2 * delta[j][t])

    # sched = np.ones((len(I), 1))
    # print(sched.shape)
    # worker_types = I

    sim = Simulation(filepath, filename, I, J, T, mu, R_mean, sigma, delta, Gamma[:, :, k], r, tau, eta, c)

    while not sim.complete:
        sim.adv_time()

    print("Ab profile: ", sim.abandonment_profile)
    print("Arrivals: ", sim.arrivals_period)


    # for filename in os.listdir(os.path.join(DATAPATH, "..", "Results", "InitialFrontier")):
    #     # print(filename)
    #     if len(filename) < 10:
    #         continue
    #     if filename[0:10] == "sched_base":
    #         print(filename)
    #         times = []
    #         overall_progress = tqdm(range(500))
    #         for k in range(500):
    #             start = time.time()
    #             overall_progress.set_description("k = %d"%k)
    #             Sim = Simulation(os.path.join(DATAPATH, "..", "Results", "InitialFrontier"), filename, I, J, T, mu, R_mean, sigma, delta, r, tau, eta, c)
    #             # progress = tqdm(T, total=34, desc="Time period")
    #             while not Sim.complete:
    #                 p1 = Sim.period
    #                 Sim.adv_time()
    #                 # if Sim.period != p1:
    #             end = time.time()
    #             times.append(end - start)
    #             overall_progress.update()
    #             overall_progress.set_postfix(percent_abandoned=round(Sim.abandonment_percentage * 100, 2))
                
    #             # progress.close()
                
    #             # print("Tot arrivals: %d"%Sim.tot_arrivals)
    #             # print("Arrivals per class: ")
    #             # for j in J:
    #             #     print("%d: %d"%(j, Sim.arrival_types[j]))
    #         overall_progress.close()
    #         print("File = ", filename)
    #         print("Average time = ", np.mean(times))

if __name__ == "__main__":
    main()