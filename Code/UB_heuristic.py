from gurobipy import *
import numpy as np
from math import floor, ceil

def ub_mip(main_mod, sub_mod, YV, I, J, S, SP, T, K, c, a, p, Y, lam, mu):
    X = {(i, s): main_mod.addVar(vtype = GRB.INTEGER) for i in I for s in S}

    main_mod.setObjective(quicksum(c[i, s] * X[i, s] for (i, s) in X), GRB.MINIMIZE)

    YLB = {(i, t): main_mod.addConstr(quicksum(a[i, s, t] * X[i, s] for s in S) >= \
            floor(YV[i, t])) 
    for i in I for t in T}

    # UB on server numbers
    ServerHeur = {i: main_mod.addConstr(quicksum(X[i, s] for s in S) <= 50)
    for i in I}

    # UB on PT servers
    PTHeur = main_mod.addConstr(quicksum(X[i, s] for i in I for s in SP) <= 4)

    main_mod.optimize()

    XUB = {(i, s): X[i, s].x for i in I for s in S}
    YUB = {(i, t): sum(a[i, s, t] * XUB[i, s] for s in S) for i in I for t in T}

    V = {(i, j): sub_mod.addVar(lb=0) for i in I for j in J}

    W = {j: sub_mod.addVar(lb=0) for j in J}

    sub_mod.setObjective(quicksum(p[j] * W[j] for j in J), GRB.MINIMIZE)

    q = {t: 0 for t in T}

    Service = {j: sub_mod.addConstr(quicksum(mu[i][j] * V[i, j] for i in I) + \
        W[j] == 0)
    for j in J}

    CapUB = {i: sub_mod.addConstr(quicksum(V[i, j] for j in J) <= 0)
    for i in I}


    for t in T:
        for i in I:
            CapUB[i].RHS = YUB[i, t]
        for k in K:
            for j in J:
                Service[j].RHS = lam[j][t][k]

            sub_mod.optimize()

            q[t] += sub_mod.objVal

        q[t] = 1/len(K) * q[t]
    
    ZUB = {t: q[t] for t in T}

    return XUB, YUB, ZUB