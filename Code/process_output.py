import numpy as np
import os
import math
import json
import matplotlib.pyplot as plt
from ast import literal_eval as make_tuple

from data import DATAPATH

STD_TYPES = types = ["Zero", "Benders Warm Start", "MIR Warm Start", "Agg MIR Warm Start",
            "Benders CB", "MIR CB", "UB"]

def model_sched(XV, I, T):
    opt_sched = np.zeros([len(I), len(T)])

    for (i, s) in XV:
        if XV[i, s] > 0.9:
            shift_start = s[0]
            shift_end = s[-1]
            opt_sched[i, shift_start:shift_end + 1] += 1
    return opt_sched

def construct_sched(XV, I, S, T):
    num_workers = [0] * len(I)
    for i in I:
        num_workers[i] = sum(XV[i, s] for s in S)

    tot_workers = int(round(sum(num_workers)))

    sched = np.zeros([tot_workers, len(T)])

    W = range(tot_workers)

    worker_types = [0] * tot_workers

    N = 0
    for i in I:
        for s in S:
            if XV[i, s] > 0.9:
                num = int(round(XV[i, s]))
                for n in range(N, N + num):
                    for t in s:
                        if t >= 0:
                            sched[n, t] = 1
                            worker_types[n] = i
                N += num

    return sched, worker_types

def save_sched(sched, worker_types, filepath, filename):
    try:
        with open(os.path.join(filepath, filename), 'w') as f:
            N = sched.shape[0]
            M = sched.shape[1]
            sched_str = 'Type \t Sched\n'
            for n in range(N):
                s = str(worker_types[n]) + '\t'
                for m in range(M):
                    s += str(sched[n, m])
                    s += ' '
                s += '\n'
                sched_str += s
            print("Sched str: ", sched_str)
            f.writelines(sched_str)
    except Exception:
        print("Oh well")

def read_sched(filepath, filename, T):
    with open(os.path.join(filepath, filename), 'r') as f:
        raw_data = f.readlines()
    N = len(raw_data) - 1

    sched = np.zeros([N, len(T)])
    worker_types = [0] * N

    for n in range(1, N + 1):
        info = raw_data[n].strip().split()
        worker_types[n - 1] = int(float(info[0]))
        shift_list = []
        for t in T:
            shift_list.append(int(float(info[t + 1])))
        sched[n - 1, :] = shift_list
    
    return sched, worker_types

def process_sched_levels(sched, worker_types, I, T):
    level_dict = {(i, t): 0 for i in I for t in T}
    for n in range(sched.shape[0]):
        for t in range(sched.shape[1]):
            level_dict[worker_types[n], t] += round(sched[n, t])
    
    return level_dict

def process_cut_cache(cut_cache, filename, types = STD_TYPES, results_dir = "Results"):
    type_counts = np.zeros(len(types))

    for t in list(cut_cache.keys()):
        for i in range(len(cut_cache[t])):
            cut_type = cut_cache[t][i][-1]
            for j in range(len(types)):
                if cut_type == types[j]:
                    type_counts[j] += 1
                    break

    lines = "********* Branch and Cut Summary ***********\n\n"
    for j in range(len(types)):
        lines += "%s: %d\n"%(types[j], type_counts[j])
    tot = sum(type_counts)
    lines += "#### TOTAL: %d ####"%tot

    with open(os.path.join(DATAPATH, "..", results_dir, filename), "w") as f:
        f.writelines(lines)

def save_json(json_dict, filename, 
    filepath = os.path.join(DATAPATH, "..", "Results", "Comparison"), indent = 2):
    if not os.path.isdir(filepath):
        os.mkdir(filepath)
    if os.path.isfile(os.path.join(filepath, filename)):
        # extrat the existing dict
        with open(os.path.join(filepath, filename), "r") as f:
            try:
                current_dict = json.load(f)
                for k in json_dict.keys():
                    if k in current_dict.keys():
                        if type(current_dict[k]) == list and type(json_dict[k]) == list:
                            for v in json_dict[k]:
                                current_dict[k].append(v)
                        elif type(current_dict[k]) == list:
                            current_dict[k].append(json_dict[k])
                        elif type(json_dict[k]) == list:
                            current_dict[k] = [current_dict[k]]
                            for v in json_dict[k]:
                                current_dict[k].append(v)
                        else:
                            current_dict[k] = [current_dict[k], json_dict[k]]
                    else:
                        current_dict[k] = json_dict[k]
                json_dict = current_dict
            except json.decoder.JSONDecodeError:
                pass    
            
    with open(os.path.join(filepath, filename), "w") as f:
        json.dump(json_dict, f, indent = indent)

def read_json(filename, 
    filepath = os.path.join(DATAPATH, "..", "Results", "Comparison")):

    with open(os.path.join(filepath, filename), "r") as f:
        json_dict = json.load(f)
    return json_dict        

def save_cache_json(cache, filename, filepath = os.path.join(DATAPATH, ".."), indent = 2):
    if not os.path.isdir(filepath):
        os.mkdir(filepath)
    if os.path.isfile(os.path.join(filepath, filename)):
        # extrat the existing dict
        with open(os.path.join(filepath, filename), "r") as f:
            try:
                current_dict = json.load(f)
                for k in current_dict.keys():
                    if k not in cache.keys():
                        cache[k] = current_dict[k]
            except json.decoder.JSONDecodeError:
                pass    
            
    trans_cache = {str(k): cache[k] for k in cache.keys()}
    with open(os.path.join(filepath, filename), "w") as f:
        json.dump(trans_cache, f, indent = indent)

def read_cache_json(filename, filepath = os.path.join(DATAPATH, "..")):
    with open(os.path.join(filepath, filename), "r") as f:
        json_dict = json.load(f)
    cache = {}
    for k in json_dict:
        v = json_dict[k]
        k = make_tuple(k)
        cache[k] = v
    return cache  

def main():
    filepath = os.path.join(DATAPATH, "..", "Results", "Comparison", "json")
    filename = "final_res.json"

    res_dict = read_json(filename, filepath = filepath)

    sched_costs_lbbd = []
    abandonments_lbbd = []
    sched_costs_mir = []
    abandonments_mir = []
    run_times_1 = []
    run_times_2 = []
    for alpha in res_dict:
        if res_dict[alpha]["runtimes"][0] != 0 and res_dict[alpha]["runtimes"][1] != 0:
            run_times_1.append(res_dict[alpha]["runtimes"][0])
            run_times_2.append(res_dict[alpha]["runtimes"][1])
            sched_costs_lbbd.append(res_dict[alpha]["schedcosts"][0])
            abandonments_lbbd.append(res_dict[alpha]["abandonmentprops"][0])
            sched_costs_mir.append(res_dict[alpha]["schedcosts"][1])
            abandonments_mir.append(res_dict[alpha]["abandonmentprops"][1])
    print(len(sched_costs_lbbd))
    print(len(abandonments_lbbd))
    print("1: ", np.mean(run_times_1))
    print("2: ", np.mean(run_times_2))
    plt.figure()
    plt.scatter(sched_costs_lbbd, abandonments_lbbd)
    plt.scatter(sched_costs_mir, abandonments_mir)
    plt.xlabel("Sched Costs")
    plt.ylabel("Abandonment")
    plt.legend(["LBM", "BCCM"])
    plt.show()

if __name__ == "__main__":
    main()