from typing import DefaultDict
from gurobipy import *
import numpy as np
from math import floor, ceil
import msvcrt
import sys
import time
import os
import collections
import itertools
import matplotlib.pyplot as plt

from data import read_data, gen_schedules, DATAPATH
from process_output import construct_sched, save_sched, process_cut_cache
from Simulation import Simulation
from Simulation2 import Simulate, sample_scens

EPS = 0.0001

def LBBD(alpha, 
    num_scen = 500, 
    logging = True, 
    time_lim = 3600,
    std_log = "", 
    max_rostered = 15, 
    cap_sched_cost = -1,
    seed = 69,
    sched_base_name = "Sched-LBBD-alpha", 
    data_filename = "AprilData.xlsx",
    filepath = os.path.join(DATAPATH, "..", "Results", "LBBD")):
    """Implement Logic Based Benders Decomposition to solve the Call Centre 
        Scheduling problem

    Args:
        alpha (float): Objective balancing coefficient
        num_scen (int, optional): Number of scenarios to sample. Defaults to 500.
        logging (bool, optional): Whether to show Gurobi log and verbose output. 
            Defaults to True.
        time_lim (int, optional): Limit on the run time. Defaults to 3600.
        std_log (str, optional): Base name for log file. Defaults to "".
        max_rostered (int, optional): Maximum number of servers of a single type
        to schedule at a single time. Defaults to 15.
        seed (int, optional): Seed for random number generator. Defaults to 69.
        sched_base_name (str, optional): Basis for the schedule file name. 
            Defaults to "Sched-LBBD-alpha".
        data_filename (str, optional): Name of the data file. Defaults to 
            "AprilData.xlsx".
        filepath ([type], optional): Path to where the results will be stored. 
            Defaults to os.path.join(DATAPATH, "..", "Results", "LBBD").

    Returns:
        XV (dict): Dictionary of solved X variables 
        YV (dict): Dictionary of solved Y variables
        ThetaV (dict): Dictionary of solved Theta Variables
        sched (ndarray): Optimal schedule
        worker_types (list): List with the type of each rostered worker in the 
            scedule
        I, J, S, T, c: data from problem instance 
        data_time (float): Time taken to extract the problem data
        model_time (float): Time taken to solve the problem
        abandonments (dict): Dictionary mapping each time to the number of 
            customers abandoned in that period
        arrivals (dict): Dictionary mapping each time to the number of 
            customers that arrived in that period
        abandonment_props (dict): Dictionary mapping each scenario and time to
            the proportion of abandonments in that period
    """
    

    results_path = filepath
    if not os.path.isdir(results_path):
        os.mkdir(results_path)

    np.random.seed(seed = seed)

    data_start = time.time()
    # extract data and parameters
    I, J, T, r, sigma, delta, mu, server_chain = read_data(data_filename)
    S, SP, a = gen_schedules(I, T)

    # cost coefficient for agent
    kappa = 0.1
    # penalty for losing customer j
    p = [1] * len(J)
    # overtime cost
    otc = 1.5
    # set of scenarios
    K = range(num_scen)

    Gamma = np.zeros([len(J), len(T), len(K)])
    for k in K:
        R = np.random.multivariate_normal(r, sigma)
        for j in J:
            for t in T:
                Gamma[j][t][k] = (R[j] ** 2 *  delta[j][t])

    N = range(max_rostered + 1)

    max_exp_flux = {(t, k): np.argmax(Gamma[:, t, k]/mu[j, j]) for t in T for k in K}

    data_end = time.time()

    # shift cost per server type
    c = {(i, s): (1 + kappa) * min(16, len(s)) + otc * max(len(s) - 16, 0)
        for i in I for s in S}

    Sim = sample_scens(I, J, T, K, mu, Gamma)

    sim_cache = {}
    cust_start = None
    serve_start = None

    t1 = time.time()
    for k in K: 
        abandonments, arrivals, sim_cache, serve_start, cust_start = Simulate(k, 
            None, Sim, sim_cache, cust_start, serve_start, max_rostered, I, J, T, K, 
            mu)
    t2 = time.time()
    # print("base sim time: ", t2 - t1)


    MP = Model("Benders Master Problem")

    if not logging:
        MP.setParam("OutputFlag", 0)

    X = {(i, s):MP.addVar(vtype = GRB.INTEGER, lb = 0) for i in I for s in S}
    Y = {(i, t): MP.addVar(vtype = GRB.INTEGER, lb = 0) for i in I for t in T}
    Z = {(n, i, t): MP.addVar(vtype = GRB.BINARY) for n in N for i in I for t in T}
    # U = {(l, t): MP.addVar(vtype = GRB.BINARY) for l in L for t in T}
    Theta = {(t, k): MP.addVar(lb = 0) for t in T for k in K}


    MP.setObjective(quicksum(c[i, s] * X[i, s] for i in I for s in S) + \
        alpha/len(K) * quicksum(Theta[t, k] for t in T for k in K), GRB.MINIMIZE)

    SchedLB = {(i, t): MP.addConstr(
        quicksum(a[i, s, t] * X[i, s] for s in S) == Y[i, t]
    )
    for i in I for t in T}

    NumServersUB = {i: MP.addConstr(
        quicksum(X[i, s] for s in S) <= 50
    )
    for i in I}

    PTUB = MP.addConstr(quicksum(X[i, s] for i in I for s in SP) <= 4)

    OneStaffLevel = {(i, t): MP.addConstr(quicksum(Z[n, i, t] for n in N) == 1)
    for i in I for t in T}

    YDepZ = {(i, t): MP.addConstr(quicksum(n * Z[n, i, t] for n in N) == Y[i, t])
    for i in I for t in T}

    # optionally set a limit on the schedule cost
    if cap_sched_cost > 0:
        MP.addConstr(
            quicksum(c[i, s] * X[i, s] for i in I for s in S) <= cap_sched_cost)

    model_start = time.time()

    num_cuts = 0

    stime = time.time()
    for k in K:
        D = {}
        D2 = {}
        Q = {(t, m, n): 0 for t in T for m in N for n in N}
        non_zero_1 = 0
        non_zero_2 = 0
        stime1 = time.time()
        if logging:
            print("Initial Cut ", k)

        for t in T:
            stime2 = time.time()
            for rho in I:
                servers = [rho, (rho + 1) % len(I)]
                mstart1 = time.time()
                for m in N:
                    level_tup = ((rho, t, m), ((rho + 1) % len(I), t, m))
                    if (k, level_tup) not in sim_cache:
                        level_dict = {
                            (rho, t): m, 
                            ((rho + 1) % len(I), t): m
                            }
                        abandonments, arrivals, sim_cache, serve_start, cust_start = Simulate(k, level_dict,
                            Sim, sim_cache, cust_start, serve_start, max_rostered, I, J, T, K, mu)

                    else:
                        abandonments, arrivals = sim_cache[k, level_tup]
                
                    D[m, m, t] = Q[t, m, m] = \
                        (abandonments[t]/arrivals[t] if arrivals[t] > 0 else 0)
                    if D[m, m, t] > 0:
                        non_zero_1 += 1
                    for m1 in N:
                        # for n1 in N:
                        D2[m1, m1, m, m, t] = (abandonments[t]/arrivals[t] 
                        if arrivals[t] > 0 else 0)
                    
                    if t > 0 and m < N[-1]:
                        for m1 in N[:-1]:
                            # for n2 in N[:-1]:
                            level_dict = {
                            (servers[0], t - 1): m1, 
                            (servers[1], t - 1): m1,
                            (servers[0], t): m,
                            (servers[1], t): m
                            }
                            abandonments, arrivals, sim_cache, serve_start, cust_start = Simulate(k, level_dict,
                            Sim, sim_cache, cust_start, serve_start, max_rostered, I, J, T, K, mu)


                            D2[m1, m1, m, m, t] = \
                                (abandonments[t]/arrivals[t] 
                                if arrivals[t] > 0 else 0)
                            if D2[m1, m1, m, m, t] > 0:
                                non_zero_2 += 1

                            if D2[m1, m1, m, m, t] <= D[m, m, t] + EPS:
                                break

                mend1 = time.time()

                if non_zero_2 > 0:  
                    cstart1 = time.time() 
                    for m in N:
                        # for n in N:
                        if t > 0 and m < N[-1]: # and n < N[-1]:
                            MP.addConstr(Theta[t, k] >= 
                                quicksum(D2[m1, m1, m, m, t] * \
                                    (Z[m1, servers[0], t - 1] + Z[m1, servers[1], t - 1])/2
                                    # U[tuple(make_level((m1, m1), servers[0], servers[1], max_rostered, I)), t - 1]
                                    for m1 in N) - \
                                quicksum(max(D2[mm, mm, m, m, t] - \
                                    D2[mm, mm, m1, m1, t] for mm in N) * 
                                    (Z[m1, servers[0], t - 1] + Z[m1, servers[1], t - 1])/2
                                    # U[tuple(make_level((m1, m1), servers[0], servers[1], max_rostered, I)), t - 1]
                                    for m1 in N[m + 1:]))

                            MP.addConstr(Theta[t, k] >= 
                                quicksum(D2[m, m, m2, m2, t] * \
                                    (Z[m2, servers[0], t] + Z[m2, servers[1], t])/2
                                    # U[tuple(make_level((m2, m2), servers[0], servers[1], max_rostered, I)), t]
                                    for m2 in N) - \
                                quicksum(max(D2[m, m, mm, mm, t] - \
                                    D2[m2, m2, mm, mm, t] for mm in N) * 
                                    (Z[m2, servers[0], t] + Z[m2, servers[1], t])/2
                                    # U[tuple(make_level((m2, m2), servers[0], servers[1], max_rostered, I)), t]
                                    for m2 in N[m + 1:]))

                            num_scen += 2

                    cend1 = time.time()
                if non_zero_1 > 0:
                    # print("Q = ", Q[t, m, m])
                    MP.addConstr(Theta[t, k] >= quicksum(Q[t, m, m] *
                    (Z[m, servers[0], t] + Z[m, servers[1], t])/2 
                        # U[tuple(make_level((m, m), servers[0], servers[1], max_rostered, I)), t]
                        for m in N))
                    num_cuts += 1
        etime1 = time.time()
    etime = time.time()
    if logging:
        print("Total initial cut time: ", etime - stime)


    MP.setParam("MIPGAP", 0.005)
    MP.setParam("LazyConstraints", 1)
    MP.setParam("Method", 2)
    MP.setParam('Seed', seed)

    MP._X = X
    MP._Y = Y
    MP._Z = Z
    # MP._U = U
    MP._Theta = Theta
    MP._BestObj = GRB.INFINITY
    MP._BestX = None
    MP._BestY = None
    MP._BestZ = None
    # MP._BestU = None
    MP._BestTheta = None

    MP._cut_cache = {}

    MP._Lazy = 0
    MP._num_cuts = num_cuts

    MP._I = I
    MP._J = J
    MP._K = K
    MP._S = S
    MP._SP = SP
    MP._T = T
    MP._N = N
    MP._max_rostered = max_rostered
    MP._max_exp_flux = max_exp_flux

    MP._c = c
    MP._alpha = alpha
    MP._a = a

    MP._mu = mu
    MP._R = R
    MP._sigma = sigma
    MP._delta = delta
    MP._Gamma = Gamma

    MP._LazyCuts = collections.defaultdict(int)

    MP._cust_start = cust_start
    MP._serve_start = serve_start
    MP._sim_cache = sim_cache
    MP._Sim = Sim

    MP.optimize(Callback)

    model_end = time.time()

    data_time = data_end - data_start
    model_time = model_end - model_start

    XV = {k:X[k].x for k in X.keys()}
    YV = {k:Y[k].x for k in Y.keys()}
    ThetaV = {k:Theta[k].x for k in Theta.keys()}

    # construct the resulting schedule- display level profiles for each period
    sched, worker_types = construct_sched(XV, I, S, T)
    if logging:
        for t in T:
            print("t = ", t)
            print("Level :", [YV[i, t] for i in I])
    # save in results dir
    save_sched(sched, worker_types, filepath, "%s%f.txt"%(sched_base_name, alpha))

    # resample 1000 new scenarios and similuate to test performance

    K_test = range(1000)
    Gamma_test = np.zeros([len(J), len(T), len(K_test)])
    for k in K_test:
        R = np.random.multivariate_normal(r, sigma)
        for j in J:
            for t in T:
                Gamma_test[j, t, k] = (R[j] ** 2 *  delta[j][t])

    Sim_test = sample_scens(I, J, T, K_test, mu, Gamma_test)
    abandonments = {k: None for k in K_test}
    arrivals = {k: None for k in K_test}
    abandonment_props = {k: 0 for k in K_test}
    levels = {(i, t): round(YV[i, t]) for i in I for t in T}
    for k in K_test:
        abandonments[k], arrivals[k], sim_cache, serve_start, cust_start = Simulate(k, 
                levels, Sim_test, sim_cache, None, None, max_rostered, I, J, T, K_test, 
                mu)
        abandonment_props[k] = (sum(abandonments[k][t] for t in T)/ \
            sum(arrivals[k][t] for t in T) if sum(arrivals[k][t] for t in T) > 0 else 0)

    if logging:
        print("Lazy Cuts: ", MP._Lazy)

        print("Objective: ", MP.objVal)

    XV = {(i, s): X[i, s].x for (i, s) in X}
    YV = {(i, t): Y[i, t].x for i in I for t in T}

    return XV, YV, ThetaV, sched, worker_types, I, J, S, T, c, data_time, \
        model_time, abandonments, arrivals, abandonment_props, MP._num_cuts



def Callback(model, where):
    if where == GRB.Callback.MIPSOL:
        XV = {k: v for (k, v) in zip(model._X.keys(), model.cbGetSolution(list(model._X.values())))}
        YV = {k: v for (k, v) in zip(model._Y.keys(), model.cbGetSolution(list(model._Y.values())))}
        # ZV = {k: v for (k, v) in zip(model._Z.keys(), model.cbGetSolution(list(model._Z.values())))}
        # UV = {k: v for (k, v) in zip(model._U.keys(), model.cbGetSolution(list(model._U.values())))}
        ThetaV = {k: v for (k, v) in zip(model._Theta.keys(), model.cbGetSolution(list(model._Theta.values())))}

        levels = {(i, t): int(YV[i, t]) for i in model._I for t in model._T}
    
        tot_abandonment = 0

        for k in model._K:
            level_dict = {(i, t): levels[i, t] for i in model._I for t in model._T}
            abandonments, arrivals, sim_cache, serve_start, cust_start = Simulate(
                k, level_dict, model._Sim, model._sim_cache, model._cust_start, model._serve_start,
                model._max_rostered, model._I, model._J, model._T, model._K,
                model._mu
            )

            model._sim_cache = sim_cache
            model._serve_start = serve_start
            model._cust_start = cust_start

            orig_abandonments = [(abandonments[t]/arrivals[t] if arrivals[t] > 0
                else 0) for t in model._T]

            for t in model._T:
                if ThetaV[t, k] < orig_abandonments[t] - EPS:
                    ThetaV[t, k] = orig_abandonments[t]

                    for i in model._I:
                        t_low = t
                        t_high = t

                        level_dict = {(i, t): levels[i, t] for i in model._I}

                        abandonments, arrivals, sim_cache, serve_start, cust_start = Simulate(
                            k, level_dict, model._Sim, model._sim_cache, model._cust_start, model._serve_start,
                            model._max_rostered, model._I, model._J, model._T, model._K,
                            model._mu
                        )

                        model._sim_cache = sim_cache
                        model._serve_start = serve_start
                        model._cust_start = cust_start

                        base_abandonment = (abandonments[t]/arrivals[t] 
                            if arrivals[t] > 0 else 0)

                        servers = [i, (i + 1) % len(model._I)]

                        while True:
                            t_low = max(t_low - 1, 0)
                            t_high = min(t_high + 1, model._T[-1])
                            t_int = range(t_low, t_high + 1)

                            # servers = [model._max_exp_flux[t, k],
                            #         (model._max_exp_flux[t, k] + 1) % len(model._I)]


                            level_dict = {}
                            for tt in t_int:
                                for i in servers:
                                    level_dict[i, tt] = levels[i, tt]

                            abandonments, arrivals, sim_cache, serve_start, cust_start = Simulate(
                                k, level_dict, model._Sim, model._sim_cache, model._cust_start, model._serve_start,
                                model._max_rostered, model._I, model._J, model._T, model._K,
                                model._mu
                            )

                            model._sim_cache = sim_cache
                            model._serve_start = serve_start
                            model._cust_start = cust_start

                            new_abandonment = (abandonments[t]/arrivals[t] 
                                if arrivals[t] > 0 else 0)

                            if new_abandonment >= orig_abandonments[t] - EPS or (t_low == 0 and t_high == model._T[-1]):
                                break
                            if t_low == 0 and t_high == model._T[-1]:
                                print("t = ", t)
                                raise Exception("Broke Window")

                        while t_high > t:
                            t_int = range(t_low, t_high)

                            # servers = [model._max_exp_flux[t, k],
                            #         (model._max_exp_flux[t, k] + 1) % len(model._I)]


                            level_dict = {}
                            for tt in t_int:
                                for i in servers:
                                    level_dict[i, tt] = levels[i, tt]
                            
                            abandonments, arrivals, sim_cache, serve_start, cust_start = Simulate(
                                k, level_dict, model._Sim, model._sim_cache, model._cust_start, model._serve_start,
                                model._max_rostered, model._I, model._J, model._T, model._K,
                                model._mu
                            )

                            model._sim_cache = sim_cache
                            model._serve_start = serve_start
                            model._cust_start = cust_start

                            abandonment = (abandonments[t]/arrivals[t] 
                                if arrivals[t] > 0 else 0)

                            if abandonment >= orig_abandonments[t] - EPS:
                                t_high -= 1
                            else:
                                break

                        while t_low < t:
                            t_int = range(t_low + 1, t_high + 1)

                            # servers = [model._max_exp_flux[t, k],
                            #         (model._max_exp_flux[t, k] + 1) % len(model._I)]


                            level_dict = {}
                            for tt in t_int:
                                for i in servers:
                                    level_dict[i, tt] = levels[i, tt]

                            abandonments, arrivals, sim_cache, serve_start, cust_start = Simulate(
                                k, level_dict, model._Sim, model._sim_cache, model._cust_start, model._serve_start,
                                model._max_rostered, model._I, model._J, model._T, model._K,
                                model._mu
                            )

                            model._sim_cache = sim_cache
                            model._serve_start = serve_start
                            model._cust_start = cust_start

                            abandonment = (abandonments[t]/arrivals[t] 
                                if arrivals[t] > 0 else 0)

                            if abandonment >= orig_abandonments[t] - EPS:
                                t_low += 1
                            else:
                                break

                        t_int = range(t_low, t_high + 1)

                        # servers = [model._max_exp_flux[t, k],
                        #             (model._max_exp_flux[t, k] + 1) % len(model._I)]

                        model._num_cuts += 1
                        model._LazyCuts[t_high + 1 - t_low] += 1

                        model.cbLazy(model._Theta[t, k] >= orig_abandonments[t] - \
                            quicksum((orig_abandonments[t] - 
                                Simulate(
                                    k, {(servers[0], t): m, (servers[1], t): n}, 
                                    model._Sim, model._sim_cache, model._cust_start, model._serve_start,
                                    model._max_rostered, model._I, model._J, model._T, model._K,
                                    model._mu, return_prop=True, prop_t = t
                                )) * \
                                    (model._Z[m, servers[0], t] + model._Z[n, servers[1], t])
                                # model._U[tuple(make_level((m, n), servers[0], servers[1], model._max_rostered, model._I)), t]
                                for m in model._N[levels[servers[0], t] + 1:] 
                                for n in model._N[levels[servers[1], t] + 1:]) - \
                            (orig_abandonments[t] - base_abandonment) * \
                            quicksum((model._Z[m, servers[0], tt] + model._Z[n, servers[1], tt])
                                # model._U[tuple(make_level((m, n), servers[0], servers[1], model._max_rostered, model._I)), tt]
                                for tt in range(t_low, t_high + 1) if tt != t
                                for m in model._N[levels[servers[0], tt] + 1:] 
                                for n in model._N[levels[servers[1], tt] + 1:]))     
        CandObj = sum(model._c[i,s] * XV[i, s] for i in model._I for s in model._S) + \
                model._alpha/len(model._K) * sum(ThetaV[t, k] for t in model._T for k in model._K)
        if CandObj < model._BestObj:
            if logging:
                print("Storing: ", model._BestObj)
            model._BestObj = CandObj
            model._BestX = model.cbGetSolution(list(model._X.values()))
            model._BestY = model.cbGetSolution(list(model._Y.values()))
            # model._BestU = model.cbGetSolution(list(model._U.values()))
            model._BestZ = model.cbGetSolution(list(model._Z.values()))
            model._BestTheta = [ThetaV[k] for k in model._Theta]
            
    if where == GRB.callback.MIPNODE and \
            model.cbGet(GRB.callback.MIPNODE_STATUS) == GRB.OPTIMAL and \
            model.cbGet(GRB.callback.MIPNODE_OBJBST) > model._BestObj + EPS:

        model.cbSetSolution(list(model._X.values()), model._BestX)
        model.cbSetSolution(list(model._Y.values()), model._BestY)
        model.cbSetSolution(list(model._Z.values()), model._BestZ)
        # model.cbSetSolution(list(model._U.values()), model._BestU)
        model.cbSetSolution(list(model._Theta.values()), model._BestTheta)

        ob = model.cbUseSolution()
        # print("ob = ", ob)

def make_level(level_pair, i1, i2, max_rostered, I):
    level = [max_rostered for _ in I]
    level[i1] = level_pair[0]
    level[i2] = level_pair[1]
    return level    
                    


def main():
    data_filename = "AprilData.xlsx"
    # alpha = 150
    alpha = 90
    cap_sched_cost = -1
    # alpha = 0
    max_rostered = 10
    XV, YV, ThetaV, sched, worker_types, I, J, S, T, c, data_time, model_time, \
        abandonments, arrivals, abandonment_props, lbbd_cuts = LBBD(alpha, num_scen=10, \
        seed = 1, max_rostered=max_rostered, cap_sched_cost = cap_sched_cost, data_filename = data_filename)

    scheds = [(i, s, XV[i, s]) for (i, s) in XV if XV[i, s] > 0.9]

    print("XVS: ", scheds)
    
    print("Sched: ", sched)
    print("Sched Cost: ", sum(c[i, s] * XV[i, s] for (i, s) in XV))

    K = range(1000)
    print("Abandonment percentage: %f %%"%(sum(abandonments[k][t] for k in K for t in T)/sum(arrivals[k][t] for k in K for t in T) * 100))

if __name__ == "__main__":
    main()
    


