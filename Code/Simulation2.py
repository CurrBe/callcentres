import random
import heapq
from math import floor, ceil

import numpy as np
from data import read_data, DATAPATH
import msvcrt
import time
from process_output import process_sched_levels, read_sched, save_cache_json
import os
import matplotlib.pyplot as plt

np.random.seed(1)

def sample_scens(I, J, T, K, mu, Gamma):
    # store details for arrivals and services for sim in the form
    # (cust_type, arrival_time, service_duration, abandon_time)
    Sim = [[] for _ in K]
    for k in K:
        queues = [[] for j in J]
        next_time = 0
        # current period
        t = floor(next_time)  
        # if no sampled arrivals, make next time Inf to avoid div 0 error
        next_arrival = [(random.expovariate(Gamma[j, t, k]) 
            if Gamma[j, t, k] > 0 else 10000) for j in J]

        # simulate arrivals and services for all |T| periods - use service time
        # mu[j, j] and adjust queue routing decisions later
        while t < T[-1]:
            prev_arr = next_time
            next_time = min(next_arrival)
            next_cust = next_arrival.index(next_time)
            service_base_time = random.expovariate(mu[next_cust, next_cust])
            other_service_base_time = random.expovariate(0.9 * mu[next_cust, next_cust])
            patience = 0.95 * mu[next_cust, next_cust]
            abandon_time = next_time + random.expovariate(patience)

            # store sampled times in cache- if no sampled arrivals for this 
            # period, wait until next period- dummy value 10000
            Sim[k].append((next_cust, next_time, service_base_time, abandon_time, other_service_base_time))
            next_arrival[next_cust] = (next_time + \
                random.expovariate(Gamma[next_cust, t, k]) 
                    if Gamma[next_cust, t, k] > 0 else 10000)
            # if we get stuck in an infinite loop, this gives output
            if min(next_arrival) - next_time < 0.000000001:
                print("NEXT ARRIVAL: j = %d, prev time = %f, next time = %f"%(next_cust, prev_arr, next_time))
                print("Gamma = ", Gamma[:, t, k])
                print("mu = ", mu)
                print("Service: ", service_base_time)

            # update time period        
            if floor(next_time) != t and floor(next_time) < T[-1]:
                for j in J:
                    if next_arrival[j] == 10000:
                        next_arrival[j] = floor(next_time) + \
                            (random.expovariate(Gamma[j, floor(next_time), k])
                            if Gamma[j, floor(next_time), k] > 0 else 10000)
            t = floor(next_time)
            
    return Sim


def Simulate(k, level_dict, Sim, sim_cache, cust_start, serve_start, 
    max_rostered, I, J, T, K, mu, return_prop = False, prop_t = -1):
    # all max level - lower specified server levels later
    level = [[max_rostered for _ in T] for i in I]
    # list of arrivals per time period
    arrivals = {t: 0 for t in T}

    ab_idx = {}
    

    if not level_dict:
        # running whole sim
        init = True
        t_list = T
    else:
        init = False
        # running subset of T
        t_list = sorted([k[1] for k in level_dict.keys()])
        for (i, t) in list(level_dict.keys()):
            level[i][t] = level_dict[i, t]
            
    # flatten levels into 1D tuple for key
    level_tup = tuple([(i, t, level[i][t]) for i in I for t in T])
    # level_tup = tuple([tuple(level[i]) for i in I])
    if (k, level_tup) in sim_cache:
        abandonments, arrivals = sim_cache[k, level_tup]
        if return_prop and prop_t >= 0:
            return (abandonments[prop_t]/arrivals[prop_t] if arrivals[prop_t] > 0 else 0)
        return abandonments, arrivals, sim_cache, serve_start, cust_start

    # this is where we store number of abandonments per period
    abandonments = {t: 0 for t in T}
    if init:
        tEnd = T[-1] + 1
    else:
        tEnd = t_list[-1] + 1

    # if starting at time 0, initialise all scheduled servers as available
    if init or t_list[0] == 0:
        t_upto = 0
        use_sim = Sim[k]
        if not cust_start:
            cust_start = {}
        if not serve_start:
            serve_start = {}
        servers = [[] for _ in I]
        for i in I:
            for n in range(level[i][0]):
                heapq.heappush(servers[i], 0)
    else:
        t_upto = t_list[0]
        try:
            use_sim = Sim[k][cust_start[k, t_upto]:]
        except Exception:
            print(cust_start.keys())
            raise Exception
        servers = [[] for _ in I]
        for i in I:
            servers[i] = list(serve_start[k, i, t_upto])
            if len(servers[i]) > 0:
                for n in range(level[i][t_upto], max_rostered):
                    # adjust server levels for these time periods
                    heapq.heappop(servers[i])
    
    # loop over all sampled arrival/service/abandonment tuples in the cached sim
    for (p, cust) in zip(use_sim, range(len(use_sim))):
        if p[1] >= tEnd:
            break
        arrivals[t_upto] += 1
        # overflow indicates that we've gone over the last period- initially false
        overflow = False
        if init:
            if p[1] >= t_upto + 1:
                t_upto += 1
                for i in I:
                    serve_start[k, i, t_upto] = tuple(servers[i])
                    cust_start[k, t_upto] = cust
        else:
            if t_upto >= T[-1]:
                overflow = True
                break
            elif p[1] >= t_upto + 1 or (p[1] < t_upto + 1 and ((servers[p[0]][0] if len(servers[p[0]]) > 0 else 10 ** 6) >= t_upto + 1 or (servers[(p[0] + 1) % len(I)][0] if len(servers[(p[0] + 1) % len(I)]) > 0 else 10 ** 6) >= t_upto + 1)):
                for i in I:
                    if level[i][t_upto + 1] > level[i][t_upto]:
                        # add servers
                        for n in range(level[i][t_upto], level[i][t_upto + 1]):
                            heapq.heappush(servers[i], t_upto + 1)
                    elif level[i][t_upto + 1] < level[i][t_upto]:
                        # remove servers
                        for n in range(level[i][t_upto + 1], level[i][t_upto]):
                            heapq.heappop(servers[i])
                t_upto += 1
        if overflow:
            # customer must abandon queue
            abandonments[T[-1]] += 1
            ab_idx[T[-1], abandonments[T[-1]]] = "Overflow"
        else:
            if len(servers[p[0]]) == 0 and len(servers[(p[0] + 1) % len(I)]) == 0:
                # no available servers to serve this customer class- must abandon
                abandonments[t_upto] += 1
                ab_idx[t_upto, abandonments[t_upto]] = "No servers"
                server = None
            elif len(servers[p[0]]) > 0 and ( 
                    len(servers[(p[0] + 1) % len(I)]) == 0 or \
                    min(servers[p[0]]) <= min(servers[(p[0]  + 1) % len(I)])):
                server = heapq.heappop(servers[p[0]])
            else:
                serve_type = (p[0] + 1) % len(I)
                server = heapq.heappop(servers[(p[0] + 1) % len(I)])
                serve_time = p[4]
            if server is not None:
                if server < p[3]:
                    # server is available before the customer abandons
                    server = max(server, p[1]) + serve_time
                    heapq.heappush(servers[serve_type], server)
                else:
                    # customer abandons before being served
                    abandonments[t_upto] += 1
                    ab_idx[t_upto, abandonments[t_upto]] = "Abandoned"
                    if serve_type == p[0]:
                        heapq.heappush(servers[p[0]], server)
                    else:
                        heapq.heappush(servers[(p[0] + 1) % len(I)], server)
    if init:
        # store states at the start of each period to use for future sims
        for t_upto in T[t_upto + 1:]:
            for i in I:
                serve_start[k, i, t_upto] = tuple(servers[i])
            cust_start[k, t_upto] = len(Sim[k])

    # store abandonment in the cache
    sim_cache[k, level_tup] = (abandonments, arrivals)

    if return_prop and prop_t >= 0:
        return (abandonments[prop_t]/arrivals[prop_t] if arrivals[prop_t] > 0 else 0)

    return abandonments, arrivals, sim_cache, serve_start, cust_start#, ab_idx


def main():
    filename = "AprilData.xlsx"

    I, J, T, r, sigma, delta, mu, server_chain = read_data(filename)

    K = range(10)

    Gamma = np.zeros([len(J), len(T), len(K)])
    lam = np.zeros([len(J), len(T), len(K)])

    for k in K:
        R = np.random.multivariate_normal(r, sigma)
        for j in J:
            for t in T:
                Gamma[j, t, k] = (R[j] ** 2 * delta[j][t])
                lam[j, t, k] = np.random.poisson(Gamma[j, t, k])

    # print("Gamma = ", Gamma[:, 18, 0])
    print("Gamma = ", Gamma)
    print("mu = ", mu)
    t1 = time.time()
    Sim = sample_scens(I, J, T, K, mu, Gamma)
    t2 = time.time()
    print("Tot sim time: ", t2 - t1)
    # print(Sim)

    print("Sim: ", [len(Sim[k]) for k in K])

    sim_cache = {}
    cust_start = None
    serve_start = None
    max_rostered = 20
    t3 = time.time()

    level_dict_1 = {(i, t): random.randint(0, 20) for i in I for t in T}
    level_dict_2 = {(i, t): max_rostered for i in I for t in T}
    tot_arrivals = {t: 0 for t in T}
    abandonments_1 = {t: 0 for t in T}
    for k in K:
        print("k = ", k)
        abandonments, arrivals, sim_cache, serve_start, cust_start = Simulate(k, 
            None, Sim, sim_cache, cust_start, serve_start, max_rostered, I, J, T, K, 
            mu)
        tot_arrivals = {t: 0 for t in T}
        abandonments_1 = {t: 0 for t in T}
        abandonments_11, arrivals_1, sim_cache, serve_start, cust_start = Simulate(k, level_dict_1,
            Sim, sim_cache, cust_start, serve_start, max_rostered, I, J, T, K, mu)
        print("Arr1: ", arrivals)
        abandonments_1 = {t: (abandonments_11[t]/arrivals_1[t] if arrivals_1[t] > 0 else 0) for t in T}
        
        for i in I:
            print("i = ", i)
            for m in range(level_dict_1[i, t] + 1, max_rostered + 1):
                for n in range(level_dict_1[(i + 1) % len(I), t] + 1, max_rostered):
                    level_dict_2 =  {(i, 9): m, ((i + 1) % len(I), 9): n}
                    abandonments_22, arrivals_2, sim_cache, serve_start, cust_start = Simulate(k, level_dict_2,
                        Sim, sim_cache, cust_start, serve_start, max_rostered, I, J, T, K, mu)
                    
                    abandonments_2 = {t: (abandonments_22[t]/arrivals_2[t] if arrivals_2[t] > 0 else 0) for t in T}
                

                    print("Ab 1: ", [abandonments_1[t] for t in range(10)])
                    print("Ab 2: ", [abandonments_2[t] for t in range(10)])

                    if abandonments_2[9] > abandonments_1[9]:
                        print("HERE:")
                        print("A11: ", abandonments_11)
                        print("A22: ", abandonments_22)
                        print("A1: ", arrivals_1)
                        print("A2: ", arrivals_2)
                        print(abandonments_1)
                        print(abandonments_2)

    print("Levels = ", [level_dict_1[i, t] for i in I for t in [9]])

if __name__ == "__main__":
    main()