import os
import numpy as np
from scipy.optimize import minimize_scalar
import random
import sys
from blackbox import search_min
import json
from pprint import pprint

from data import DATAPATH, read_data
from LBBD_mod import LBBD
from BaseMIP import base_mip
from process_output import read_sched, save_json
from Simulation import Simulation
from Simulation2 import sample_scens


def compare_models(alphas, cap_sched_costs, 
    num_scen_lbbd = 30, 
    num_scen_base = 500,
    num_scen_test = 300,
    logging = True, 
    time_lim = 3600,
    std_log = "", 
    max_rostered = 20, 
    seed = 42, 
    results_filename = "CompResults.txt",
    data_filename = "AprilData.xlsx",
    filepath = os.path.join(DATAPATH, "..", "Results", "Comparison")):
    """Compare the LBBD model to the MIR-Enhanced Benders base model

    Args:
        alphas (list): List of test values for alpha
        cap_sched_costs (list): List of capped schedule costs, set to [-1] if you don't want this feature
        num_scen_lbbd (int, optional): Number of scenarios to use for LBBD. Defaults to 30.
        num_scen_base (int, optional): Number of scenarios to use for base model. Defaults to 500.
        num_scen_test (int, optional): Number of scenarios to test schedules on. Defaults to 300.
        logging (bool, optional): Whether or not to show helpful output. Defaults to True.
        time_lim (int, optional): Limit on runtime for models. Defaults to 3600.
        std_log (str, optional): Basis name for file where logs are saved. Defaults to "".
        max_rostered (int, optional): Max number of any staff of same type rostered at same time. Defaults to 20.
        seed (int, optional): Seed passed to random number generator. Defaults to 42.
        results_filename (str, optional): Name of file to save results. Defaults to "CompResults.txt".
        data_filename (str, optional): Name of file containing data. Defaults to "AprilData.xlsx".
        filepath ([type], optional): Path of results dir. Defaults to os.path.join(DATAPATH, "..", "Results", "Comparison").

    Returns:
        res_dict (dict): Dictionary contaioning values for run time, sched cost, abandonment and num cuts
    """
    if not os.path.isdir(filepath):
        os.mkdir(filepath)

    run_times = {str((alpha, cap_sched_cost)): 
        [0, 0] for alpha in alphas for cap_sched_cost in cap_sched_costs}
    sched_costs = {str((alpha, cap_sched_cost)): 
        [0, 0] for alpha in alphas for cap_sched_cost in cap_sched_costs}
    abandonment_dict = {str((alpha, cap_sched_cost)): 
        [0, 0] for alpha in alphas for cap_sched_cost in cap_sched_costs}
    num_cuts = {str((alpha, cap_sched_cost)): 
        [0, 0] for alpha in alphas for cap_sched_cost in cap_sched_costs}


    sched_base_orig = "Sched-alpha-"
    sched_base_lbbd = "Sched-LBBD-alpha-"

    K_test = range(num_scen_test)

    K = range(num_scen_lbbd)
    I, J, T, r, sigma, delta, mu, server_chain = read_data(data_filename)

    Gamma = np.zeros([len(J), len(T), len(K)])
    for k in K:
        R = np.random.multivariate_normal(r, sigma)
        for j in J:
            for t in T:
                Gamma[j][t][k] = (R[j] ** 2 *  delta[j][t])
                
    Sim = sample_scens(I, J, T, K, mu, Gamma)
        
    for a in range(len(alphas)):
        for cap_sched_cost in cap_sched_costs:
            print("******** a = %d, alpha = %5.2f, cap sched cost = %5.2f *************"%(a, alphas[a], cap_sched_cost))
            alpha = alphas[a]
            XV, YV, ThetaV, sched, worker_types, I, J, S, T, c, data_time, model_time, \
            abandonments, arrivals, abandonment_props, lbbd_cuts = LBBD(alpha, num_scen=num_scen_lbbd, \
                logging = logging, time_lim = time_lim, std_log = std_log, 
                max_rostered = max_rostered, cap_sched_cost = cap_sched_cost,
                seed = seed, Sim = Sim, Gamma = Gamma, sched_base_name = sched_base_lbbd,
                data_filename = data_filename, filepath = filepath)

            abandonment_props = {k: (sum(abandonments[k][t] for t in T)/sum(arrivals[k][t] for t in T) if sum(arrivals[k][t] for t in T) > 0 else 0) for k in K_test}
            tot_abandonment_lbbd = sum(abandonment_props[k] for k in K_test)/len(K_test) * 100
            print("Abandonment: ", tot_abandonment_lbbd)

            run_times[str((alpha, cap_sched_cost))][0] = model_time
            sched_costs[str((alpha, cap_sched_cost))][0] = sum(c[i, s] * XV[i, s] for i in I for s in S)
            abandonment_dict[str((alpha, cap_sched_cost))][0] = tot_abandonment_lbbd
            num_cuts[str((alpha, cap_sched_cost))][0] = lbbd_cuts

            I, J, T, R_mean, sigma, delta, mu, server_chain = read_data(data_filename)

            XV, YV, I, J, S, T, c, data_time, model_time, cut_cache = base_mip(data_filename, 
                alpha/100, num_scen = num_scen_base, logging = logging, 
                time_lim = time_lim, std_log = std_log, seed = seed, 
                sched_base_name = sched_base_orig, filepath = filepath, cap_sched_cost = cap_sched_cost)

            K_test = range(num_scen_test)
            Gamma_test = np.zeros([len(J), len(T), len(K_test)])
            R = {k: None for k in K}
            for k in K_test:
                R[k] = np.random.multivariate_normal(R_mean, sigma)
                for j in J:
                    for t in T:
                        Gamma_test[j, t, k] = (R[k][j] ** 2 *  delta[j][t])
            sched, worker_types = read_sched(filepath, "%s%f.txt"%(sched_base_orig, alpha/100), T)
            sched_abandonments = {(k, t): 0 for k in K_test for t in T}
            sched_arrivals = {(k, t): 0 for k in K_test for t in T}

            r_tand = 100
            tau = 1
            eta = 0.01
            c_tand = 2
            for k in K_test:
                sim = Simulation("", "", I, J, T, mu, R_mean, sigma, delta, 
                Gamma_test[:, :, k], r_tand, tau, eta, c_tand, sched = sched, 
                worker_types = worker_types)

                while not sim.complete:
                    sim.adv_time()

                for t in T:
                    sched_abandonments[k, t] = sim.abandonment_profile[t]
                    sched_arrivals[k, t] = sim.arrivals_period[t]

            tot_abandonment_base = 100 * (sum(sched_abandonments[k, t] for k in K_test for t in T)/sum(sched_arrivals[k, t] for k in K_test for t in T)
                if sum(sched_arrivals[k, t] for k in K_test for t in T) > 0 else 0)

            print("Abandonment: ", tot_abandonment_base)

            run_times[str((alpha, cap_sched_cost))][1] = model_time
            sched_costs[str((alpha, cap_sched_cost))][1] = sum(c[i, s] * XV[i, s] for i in I for s in S)
            abandonment_dict[str((alpha, cap_sched_cost))][1] = tot_abandonment_base
            num_cuts[str((alpha, cap_sched_cost))][1] = sum(len(cut_cache[t]) for t in T)

            res_dict = {str((alpha, cap_sched_cost)): 
                {
                "runtimes": run_times[str((alpha, cap_sched_cost))],
                "schedcosts": sched_costs[str((alpha, cap_sched_cost))],
                "abandonmentprops": abandonment_dict[str((alpha, cap_sched_cost))],
                "numcuts": num_cuts[str((alpha, cap_sched_cost))]
                }
                for alpha in alphas for cap_sched_cost in cap_sched_costs
            }

            save_json(res_dict, "CompResults.json", os.path.join(filepath, "json"))

    return res_dict


def main():
    alphas = np.linspace(20, 250, 20)
    cap_sched_costs = [-1]
    num_scen_lbbd = 30
    num_scen_base = 500
    num_scen_test = 1000
    time_lim = 3600

    results = compare_models(
        alphas, cap_sched_costs, num_scen_lbbd = num_scen_lbbd, 
        num_scen_base = num_scen_base,num_scen_test = num_scen_test,
        time_lim = time_lim, results_filename = "LBBDActuallyGoodResults.json"
    )

    pprint(results)

if __name__ == "__main__":
    main()