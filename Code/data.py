import xlrd
import os
import numpy as np
from math import floor, ceil
import matplotlib.pyplot as plt

# THESE ARE ALL PATHS TO SUIT MY PC(s)- YOU WILL HAVE TO CHANGE THEM IF YOU WANT THIS TO RUN
USER = "User"
# USER = "S4480414"
# USER = "CurrB"
DATAPATH = os.path.join("C:", "\\Users", USER, "OneDrive", "Documents", "2020Honours", "CallCentres", "Data")


def read_data(filename):
    filepath = os.path.join(DATAPATH, filename)
    wb = xlrd.open_workbook(filepath)
    sheet = wb.sheet_by_index(0)

    J = range(6)
    I = range(6)
    T = range(34)
    D = range(30)
    W = {d: floor(d/7) for d in D}

    nJ = len(J)
    nI = len(I)
    nT = len(T)

    customers = ["PS", "PE", "IN", "NE", "NW", "TT"]

    types = []
    for value in sheet.col_values(4)[1:]:
        if str(value) in customers:
            types.append(customers.index(str(value)))
        else:
            types.append(-1)
    
    periods = [floor(value * 24 * 2 - 14) for value in sheet.col_values(6)[1:]]

    days = [int(value - 990401) for value in sheet.col_values(5)[1:]]
    
    N = range(len(periods))

    arrivals_day = np.zeros([nJ, len(D)])
    arrivals_period = np.zeros([nJ, nT])

    arrivals_period_day = np.zeros([len(D), nJ, nT])

    for n in N:
        if types[n] != -1:
            arrivals_day[types[n]][days[n]]+= 1
            arrivals_period[types[n]][periods[n]] += 1

            arrivals_period_day[days[n]][types[n]][periods[n]] += 1

    # arr_dat = arrivals_period_day
    arrivals_day = arrivals_day ** (1/2)

    arrivals_period = arrivals_period ** (1/2)

    r = np.mean(arrivals_day, axis=1)

    sigma = np.cov(arrivals_day)
    
    delta = ((arrivals_period.T / np.sum(arrivals_period, axis = 1)).T)

    outcomes = [value for value in sheet.col_values(12)[1:]]

    outcome_agent = []
    for n in N:
        if types[n] != -1 and outcomes[n] == "AGENT":
            outcome_agent.append(1)
        else:
            outcome_agent.append(0)
    
    service_times = [int(value) for value in sheet.col_values(15)[1:]]

    # estimate of service rate for each class
    alpha = [sum(outcome_agent[n] for n in N if types[n] == j)/sum(service_times[n] for n in N if types[n] == j) * 1800 for j in J]

    mu = np.zeros([nI, nJ])   

    for i in I:
        for j in J:
            if i == (j + 1) % nJ:
                mu[i][j] = 0.9 * alpha[j]
            elif i == j:
                mu[i][j] = alpha[j]

    # define the server chain specified in paper
    server_chain = {i: [(i - 1) % nI, i] for i in I}

    return I, J, T, r, sigma, delta, mu, server_chain#, arr_dat

def gen_schedules(I, T):
    # of the form [FT, OT, PT]
    shift_lens = [[16], [17, 18, 19], [6, 7, 8, 9, 10]]
    # for FT and OT only
    allowed_breaks = [[7, 8, 9], [7, 8, 9], []]

    # shifts of the form [1 if working in period, 0 if break in period]
    scheds = set()
    ptscheds = set()
    for stype in range(3):
        for slen in shift_lens[stype]:
            # for start in range(len(T) - slen):
            for start in T:
                if start + slen > len(T):
                    break
                if stype == 2:
                    shift = range(start, start + slen)
                    scheds.add(tuple(shift))
                    ptscheds.add(tuple(shift))
                else:
                    for sbreak in allowed_breaks[stype]:
                        shift = list(range(start, start + slen))
                        shift[sbreak] = -1
                        scheds.add(tuple(shift))

                    
    SF = list(scheds)
    SP = list(ptscheds)

    a = {(i, s, t): (1 if t in s else 0) for i in I for s in SF + SP for t in T}

    return SF, SP, a



def main():
    filename = "AprilData.xlsx"

    np.random.seed(seed = 69)

    I, J, T, r, sigma, delta, mu, server_chain = read_data(filename)

    K = range(50)

    Gamma = np.zeros([len(K), len(J), len(T)])
    lam = np.zeros([len(K), len(J), len(T)])

    for k in K:
        R = np.random.multivariate_normal(r, sigma)
        for j in J:
            for t in T:
                Gamma[k][j][t] = (R[j] ** 2 * delta[j][t])
                lam[k][j][t] = np.random.poisson(Gamma[k][j][t])

    print(mu)

    ymax = ceil(np.percentile(Gamma, 99)/np.percentile(mu[mu != 0], 1))
    print("ymax = ", ymax)

    mean_lam = np.mean(lam, axis = 0)
    plt.title("New way")
    for j in J:
        plt.scatter([t for k in K for t in T], [lam[k][j][t] for k in K for t in T])
        plt.plot(T, mean_lam[j])   
    plt.legend(J)
    plt.show()

if __name__ == "__main__":
    main()