import numpy as np

def pad_array(x):
    x = np.array(x)
    if len(x.shape) == 1:
        max_len = max([len(x[i]) for i in range(len(x))])
        for i in range(len(x)):
            if len(x[i]) < max_len:
                x[i] = x[i] + [np.Inf] * (max_len - len(x[i]))
    x = np.array(list(x))
    return x