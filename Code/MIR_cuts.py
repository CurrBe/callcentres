import numpy as np
from math import floor, ceil

from Benders_cuts import scaled_violation_benders

def MIR_benders(ZV, YV, Z, Y, d1, c_cut, cut_cache, t, I):
    most_violated_d = []
    max_violation = 0
    most_violated_c = -1
    for c in range(len(cut_cache)):
        d2 = cut_cache[c][0]
        if d2 == d1:
            continue
        elif d1[0] > d2[0]:
            # tmp = d2
            # d2 = d1
            # d1 = tmp
            continue
        # beta = [1]
        beta = np.zeros([len(I) + 1, 1])
        beta[0] = 1
        for i in I:
            if d1[i] != d2[i]:
                beta[i + 1] = 1/abs(d2[i] - d1[i])
        for n in range(len(beta)):
            if beta[n] != 0:
                f0 = beta[n] * (d2[0] - d1[0]) - floor(beta[n] * (d2[0] - d1[0]))
                if f0 > 0:
                    f = [beta[n] * (d2[i + 1] - d1[i + 1]) - floor(beta[n] * \
                        (d2[i + 1] - d1[i + 1])) for i in I]
                    db = np.zeros([len(I) + 1, 1])
                    db[0] = d1[0] + (f0 * ceil(beta[n] * (d2[0] - d1[0]))/beta[n])
                    for i in I:
                        db[i + 1] = min(f0 * ceil(beta[n] * (d2[i + 1] - \
                            d1[i + 1])), f[i] + f0 * floor(beta[n] * \
                            (d2[i + 1] - d1[i + 1])))/beta[n] + d1[i + 1]
                    violation = scaled_violation_benders(ZV, YV, db, t, I)
                    if violation > max_violation:
                        most_violated_d = db
                        max_violation = violation
                        most_violated_c = c
                    else:
                        continue
    if len(most_violated_d) > 0:
        most_violated_d = list(most_violated_d.flatten())
    return most_violated_d, max_violation, most_violated_c

def process_scen_MIR(I, J, k, t, gammas, pis, lam):
    d0 = sum(gammas[j, k] * lam[j][t][k] for j in J)
    d = tuple([d0] + [pis[i, k] for i in I])
    return d

def MIR_scen(Q, YV, d1, scen_cut_cache, t, k, I):
    most_violated_d = []
    max_violation = 10 ** -5
    most_violated_c = -1

    cut_list = list(scen_cut_cache[t, k])
    for c in range(len(cut_list)):
        d2 = cut_list[c]
        if d2 == d1:
            continue
        elif d1[0] > d2[0]:
            tmp = d2
            d2 = d1
            d1 = tmp
        beta = np.zeros([len(I) + 1, 1])
        beta[0] = 1
        for i in I:
            if d1[i] != d2[i]:
                beta[i + 1] = 1/abs(d2[i] - d1[i])
            for n in range(len(beta)):
                if beta[n] != 0:
                    f0 = beta[n] * (d2[0] - d1[0]) - floor(beta[n] * (d2[0] - d1[0]))
                    if f0 > 0:
                        f = [beta[n] * (d2[i + 1] - d1[i + 1]) - floor(beta[n] * \
                            (d2[i + 1] - d1[i + 1])) for i in I]
                        db = np.zeros([len(I) + 1, 1])
                        db[0] = d1[0] + (f0 * ceil(beta[n] * (d2[0] - d1[0]))/beta[n])
                        for i in I:
                            db[i + 1] = min(f0 * ceil(beta[n] * (d2[i + 1] - \
                                d1[i + 1])), f[i] + f0 * floor(beta[n] * \
                                (d2[i + 1] - d1[i + 1])))/beta[n] + d1[i + 1]
                        violation = scaled_violation_scen(Q, YV, db, t, k, I)
                        if violation > max_violation:
                            most_violated_d = db
                            max_violation = violation
                            most_violated_c = c
    if len(most_violated_d) > 0:
        most_violated_d = list(most_violated_d.flatten())
    

    benders_violation = scaled_violation_scen(Q, YV, d1, t, k, I)

    if abs(benders_violation) > abs(max_violation) or len(most_violated_d) == 0:
        most_violated_d = d1
        max_violation = benders_violation
        most_violated_c = -1

    return most_violated_d, max_violation, most_violated_c

def scaled_violation_scen(Q, YV, d, t, k, I):
    violation = max(d[0] + sum(d[i + 1] * YV[i, t] for i in I) - Q, 0)
    scaled_v = violation/np.linalg.norm(np.array([1] + list(d)))
    return scaled_v
    
def most_violated_list(YV, Q, scen_cut_list, t, I):
    max_violated = -np.inf
    max_c = -1
    for c in range(len(scen_cut_list)):
        d = scen_cut_list[c]
        try:
            violation = d[0] + sum(d[i + 1] * YV[i, t] for i in I) - Q[c]
        except Exception:
            print(c)
            print(len(scen_cut_list))
            print(d)
            print(Q)
            print(len(d))
            print(len(Q))
            raise Exception
        if violation > max_violated:
            max_violated = violation
            max_c = c
    return max_c

def aggregate_cuts(scen_cut_list, YV, Q, Y, Z, t, I):
    c_cut = most_violated_list(YV, Q, scen_cut_list, t, I)
    d1 = scen_cut_list[c_cut]
    most_violated_d = []
    max_violation = 0
    most_violated_c = -1

    for c in range(len(scen_cut_list)):
        d2 = scen_cut_list[c]
        if d2 == d1:
            continue
        elif d1[0] > d2[0]:
            tmp = d2
            d2 = d1
            d1 = tmp
        beta = np.zeros([len(I) + 1, 1])
        beta[0] = 1
        for i in I:
            if d1[i] != d2[i]:
                beta[i + 1] = 1/abs(d2[i] - d1[i])
            for n in range(len(beta)):
                if beta[n] != 0:
                    f0 = beta[n] * (d2[0] - d1[0]) - floor(beta[n] * (d2[0] - d1[0]))
                    if f0 > 0:
                        f = [beta[n] * (d2[i + 1] - d1[i + 1]) - floor(beta[n] * \
                            (d2[i + 1] - d1[i + 1])) for i in I]
                        db = np.zeros([len(I) + 1, 1])
                        db[0] = d1[0] + (f0 * ceil(beta[n] * (d2[0] - d1[0]))/beta[n])
                        for i in I:
                            db[i + 1] = min(f0 * ceil(beta[n] * (d2[i + 1] - \
                                d1[i + 1])), f[i] + f0 * floor(beta[n] * \
                                (d2[i + 1] - d1[i + 1])))/beta[n] + d1[i + 1]
                        violation = max(db[0] + sum(db[i] * YV[i, t] for i in I) - Q[c], 0)
                        violation = violation/np.linalg.norm(np.array([1] + list(db)))
                        if violation > max_violation:
                            most_violated_d = db
                            max_violation = violation
                            most_violated_c = c
                    else:
                        continue
    if len(most_violated_d) > 0:
        most_violated_d = list(most_violated_d.flatten())

    d1_violation = max(d1[0] + sum(d1[i] * YV[i, t] for i in I) - Q[c_cut], 0)
    d1_violation = d1_violation/np.linalg.norm(np.array([1] + list(d1)))

    if d1_violation > max_violation or len(most_violated_d) == 0:
        most_violated_d = d1
        max_violation = d1_violation
        most_violated_c = c_cut

    return most_violated_d, max_violation, most_violated_c

def multi_cut_MIR(YV, Q, d1, multi_cut_cache, t, k, I):
    most_violated_d = []
    max_violation = 0
    most_violated_c = -1

    cut_list = list(multi_cut_cache[t, k])

    for c in range(len(cut_list)):
        d2 = cut_list[c]

        if d2 == d1:
            continue
        elif d1[0] > d2[0]:
            tmp = d2
            d2 = d1
            d1 = d2
        beta = np.zeros([len(I) + 1, 1])
        beta[0] = 1
        for i in I:
            if d1[i] != d2[i]:
                beta[i + 1] = 1/abs(d2[i] - d1[i])
        for n in range(len(beta)):
            if beta[n] != 0:
                f0 = beta[n] * (d2[0] - d1[0]) - floor(beta[n] * (d2[0] - d1[0]))
                if f0 > 0:
                    f = [beta[n] * (d2[i + 1] - d1[i + 1]) - floor(beta[n] * \
                        (d2[i + 1] - d1[i + 1])) for i in I]
                    db = np.zeros([len(I) + 1, 1])
                    db[0] = d1[0] + (f0 * ceil(beta[n] * (d2[0] - d1[0]))/beta[n])
                    for i in I:
                        db[i + 1] = min(f0 * ceil(beta[n] * (d2[i + 1] - \
                            d1[i + 1])), f[i] + f0 * floor(beta[n] * \
                            (d2[i + 1] - d1[i + 1])))/beta[n] + d1[i + 1]

                    violation = max(db[0] + sum(db[i] * YV[i, t] for i in I) - Q, 0)
                    violation = violation/np.linalg.norm(np.array([1] + list(db)))
                    if violation > max_violation:
                        most_violated_d = db
                        max_violation = violation
                        most_violated_c = c
                    else:
                        continue
    if len(most_violated_d) > 0:
        most_violated_d = list(most_violated_d.flatten())
    return most_violated_d, max_violation, most_violated_c