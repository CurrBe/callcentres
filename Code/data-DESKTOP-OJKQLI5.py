import xlrd
import os
import numpy as np
from math import floor
import matplotlib.pyplot as plt

USER = "CurrB"
# USER = "S4480414"
DATAPATH = os.path.join("C:", "\\Users", USER, "OneDrive", "Documents", "2020Honours", "CallCentres", "Data")


def read_data(filename):
    filepath = os.path.join(DATAPATH, filename)
    wb = xlrd.open_workbook(filepath)
    sheet = wb.sheet_by_index(0)

    J = range(6)
    I = range(6)
    T = range(34)
    D = range(30)

    nJ = len(J)
    nI = len(I)
    nT = len(T)

    customers = ["PS", "PE", "IN", "NE", "NW", "TT"]

    types = []
    for value in sheet.col_values(4)[1:]:
        if str(value) in customers:
            types.append(customers.index(str(value)))
        else:
            types.append(-1)
    
    periods = [floor(value * 24 * 2 - 14) for value in sheet.col_values(6)[1:]]

    # print([value for value in sheet.col_values(6)[1:]])
    days = [int(value - 990401) for value in sheet.col_values(5)[1:]]
    
    N = range(len(periods))

    arrivals_day = np.zeros([nJ, len(D)])
    arrivals_period = np.zeros([nJ, nT])

    for n in N:
        if types[n] != -1:
            arrivals_day[types[n]][days[n]]+= 1
            arrivals_period[types[n]][periods[n]] += 1

    # arrivals_day[1:] = arrivals_day[1:]
    # arrivals_day[0] = arrivals_day[0]

    arrivals_day = arrivals_day ** (1/2)

    r = np.mean(arrivals_day, axis=1)

    sigma = np.cov(arrivals_day)

    # print(np.diag(sigma))
    
    delta = (arrivals_period.T / np.sum(arrivals_period, axis = 1))

    # print(np.sum(arrivals_period, axis=1))
    # print(np.sum(arrivals_period, axis=0))

    outcomes = [value for value in sheet.col_values(12)[1:]]

    outcome_agent = []
    for n in N:
        if types[n] != -1 and outcomes[n] == "AGENT":
            outcome_agent.append(1)
        else:
            outcome_agent.append(0)
    
    service_times = [int(value) for value in sheet.col_values(15)[1:]]

    # estimate of service rate for each class
    alpha = [sum(outcome_agent[n] for n in N if types[n] == j)/sum(service_times[n] for n in N if types[n] == j) * 1800 for j in J]

    mu = np.zeros([nI, nJ])   

    for i in I:
        for j in J:
            if i == (j + 1) % nJ:
                mu[i][j] = 0.9 * alpha[j]
            elif i == j:
                mu[i][j] = alpha[j]

    # define the server chain specified in paper
    server_chain = {i: [(i - 1) % nI, i] for i in I}

    return I, J, T, r, sigma, delta, mu, server_chain

def gen_schedules(I, T):
    # of the form [FT, OT, PT]
    shift_lens = [[16], [17, 18, 19], [6, 7, 8, 9, 10]]
    # for FT and OT only
    allowed_breaks = [[7, 8, 9], [7, 8, 9], []]

    # shifts of the form [1 if working in period, 0 if break in period]
    scheds = set()
    ptscheds = set()
    for stype in range(3):
        for slen in shift_lens[stype]:
            # for start in range(len(T) - slen):
            for start in T:
                if start + slen > len(T):
                    break
                if stype == 2:
                    shift = range(start, start + slen)
                    scheds.add(tuple(shift))
                    ptscheds.add(tuple(shift))
                else:
                    for sbreak in allowed_breaks[stype]:
                        shift = list(range(start, start + slen))
                        shift[sbreak] = -1
                        scheds.add(tuple(shift))

                    
    SF = list(scheds)
    SP = list(ptscheds)

    a = {(i, s, t): (1 if t in s else 0) for i in I for s in SF + SP for t in T}

    return SF, SP, a



def main():
    filename = "AprilData.xlsx"

    I, J, T, r, sigma, delta, mu, server_chain = read_data(filename)

    print(r ** 2)
    # S, SP, a = gen_schedules(I, T)

    K = range(10000)

    Gamma = np.zeros([len(K), len(J), len(T)])
    lam = np.zeros([len(K), len(J), len(T)])

    for k in K:
        R = np.random.multivariate_normal(r, sigma)
        # Gamma = np.zeros([len(J), len(T)])
        for j in J:
            for t in T:
                Gamma[k][j][t] = R[j]  ** 2 * delta[t][j]
                lam[k][j][t] = np.random.poisson(Gamma[k][j][t])

    # mean_lam = np.mean(lam, axis = 0)
    # for j in J:
    #     plt.scatter(T, mean_lam[j])

    t = 10
    for j in J:
        m = np.mean([lam[k][j][t] for k in K], axis = 0)
        print(m)
        plt.plot(K, [m for _ in K])
        plt.scatter(K, [lam[k][j][t] for k in K])

    # print("r = ", r)
    # mean_Gamma = np.mean(Gamma, axis=0)
    # for j in J:
    #     print("j")
    #     print("delta = ", delta[0:-1][j])
    #     plt.scatter(T, mean_Gamma[j])
    
    # plt.legend(J)
            # plt.scatter(T, Gamma[0])
    
    # for j in J:
    #     print("j = ", j)
    #     print("sum delta = ", sum(delta[t][j] for t in T))
    plt.show()
        # lam = np.zeros([len(J), len(T)])
        # for j in J:
        #     for t in T:
        #         lam[j][t] = np.random.poisson(Gamma[j][t])
            
    # print("r = ", r)
    print("Sigma = ", sigma)
    # print("R = ", R)
    # print("delta = ", delta)
    # print("G = ", Gamma)
    # print("sum = ", [sum(delta[t][j] for t in T) for j in J])
    # print("lam = ", lam)

    # for t in T:
    #     plt.scatter()


    # for t in T:
    #     for j in J[:1]:
    #         lams = []
    #         print("*** t = %d, j = %d ***"%(t, j))
    #         print("Gamma = ", Gamma[j][t])
            # for k in range(1000):
            #     # print("lam = ", np.random.poisson(Gamma[j][t]))

            #     lams.append(np.random.poisson(Gamma[j][t]))
                
            # m = np.average(lams)
            # s = np.std(lams)

            # print("*** t = %d, j = %d ***"%(t, j))
            # print("mean = ", m)
            # print("standard dev = ", s)

    # print("mu = ", mu)

    # S, SP, a = gen_schedules(I, T)
    # print(S)

if __name__ == "__main__":
    main()