import os
from tqdm import *
import matplotlib.pyplot as plt
import numpy as np
import win32com.client as win32
from random import randint

from BaseMIP import base_mip
from process_output import construct_sched, save_sched, process_cut_cache
from Simulation import Simulation
from data import read_data, DATAPATH
from full_mip import full_mip


def efficient_frontier(alphas, num_scen=500, results_dir = "Results", file_base_name="", \
    data_file="AprilData.xlsx", logging=True, time_lim = 900, std_log = "", \
    cut_file_base_name = "", mode="Decomp", show = True):
    """
    Calculate the efficient frontier for a given list of alpha values

    Parameters
    ----------
    `alphas` : iterable
        List of alpha values

    `num_scen` : int, optional
        Number of scenarios to solve for each instance, by default 500

    `results_dir` : str, optional
        Directory where results will be stored, by default "Results"

    `file_base_name` : str, optional
        Prefix for the file name where results are stored, by default ""

    `logging` : bool, optional
        Whether or not to show Gurobi logging, by default True

    `time_lim` : int, optional
        Maximum solve time for SIP model, by default 900

    `std_log` : str, optional
        Name of the file prefix where logging is stored, by default ""
        
    `mode` : str, optional
        Indicates whether to solve with SIP ("Decomp") or extended ("full") 
        models, by default "Decomp"

    `show` : bool, optional
        Whether to show the plot of the efficient frontier, by default True

    Returns
    -------
    `sched_costs` : list
        List of schedule costs for each alpha value
    
    `abandonment_approx` : list
        List of approximation of abandonment percentages for each alpha

    Raises
    ------
    Exception
        Error in saving schedule
    """    

    if file_base_name == "":
        file_base_name = "sched_base_alpha"

    if cut_file_base_name == "":
        cut_file_base_name = "cut_cache_alpha"
    
    results_path = os.path.join(DATAPATH, "..", results_dir, "ActualResults")
    if not os.path.isdir(results_path):
        os.mkdir(results_path)

    # load data
    I, J, T, R_mean, sigma, delta, mu, server_chain = read_data(os.path.join(DATAPATH, data_file))

    # constants for SHADOW-TANDEM
    r = 100
    tau = 1
    eta = 0.01
    c = 2
    
    sched_costs = [0] * len(alphas)
    abandonment_percentages = np.zeros((len(alphas), num_scen))

    abandonment_approx = np.zeros(len(alphas))

    overall_progress = tqdm(range(len(alphas)))
    
    for a in overall_progress:
        overall_progress.set_description("alpha = %f"%alphas[a])
        filename = file_base_name + "%f.txt"%alphas[a]
        cut_filename = cut_file_base_name + "%f.txt"%alphas[a]

        cut_cache = None

        if mode == "Decomp":
            XV, YV, I, J, S, T, c_mip, data_time, model_time, cut_cache = base_mip(data_file, alphas[a], 
                num_scen = num_scen, logging=logging, time_lim = time_lim, \
                results_dir = results_dir, std_log = std_log, filepath = results_path)
            process_cut_cache(cut_cache, cut_filename, results_dir = results_dir)
        else:
            XV, YV, I, J, S, T, c_mip = full_mip(data_file, alphas[a],
                logging=logging)

        try:
            # construct schedule and save
            sched, worker_types = construct_sched(XV, YV, I, J, S, T)
            save_sched(sched, worker_types, results_path, filename)
        except Exception:
            print(sched)
            print(worker_types)
            print(filename)

        sched_costs[a] = sum(c_mip[i, s] * XV[i, s] for (i, s) in XV)

        # begin simulation
        K = range(num_scen)
        progress = tqdm(T, total=num_scen, desc="Simulation")
        for k in K:
            Sim = Simulation(results_path, filename, I, J, T, mu, R_mean, sigma, delta, r, tau, eta, c)
            while not Sim.complete:
                p1 = Sim.period
                Sim.adv_time()
            abandonment_percentages[a, k] = Sim.abandonment_percentage
            progress.update()
            progress.set_postfix(percent_abandoned=round(Sim.abandonment_percentage * 100, 2))
        progress.close()

        abandonment_approx[a] = np.mean(abandonment_percentages[a, :])

        # attempt to save results
        try:
            save_frontier(alphas, sched_costs, abandonment_approx, results_path, "EfficientFrontierResults.txt", overwrite = True)
        except Exception:
            print("alphas = ", alphas)
            print("sched costs = ", sched_costs)
            print("abandonment approx = ", abandonment_approx)
            print("results path = ", results_path)
            raise Exception
        # outlook = win32.Dispatch('outlook.application')
        # mail = outlook.CreateItem(0)
        # mail.To = "CurrBe98@outlook.com"
        # mail.Subject = "Call Centre Efficient Frontier Progress Report"
        # mail.Body = "Current Data from execution"
        # attachment = os.path.join(results_path, "EfficientFrontierResults.txt")
        # mail.Attachments.Add(attachment)
        # mail.Send()
    overall_progress.close()

    if show:
        # plot efficient frontier
        plt.scatter(sched_costs, abandonment_approx)
        plt.xlabel("Schedule Cost")
        plt.ylabel("Abandonment Percentage")
        plt.title("Efficient Frontier")
        plt_filename = "EfficientFrontier-K%d-A-%d.png"%(len(K), len(alphas))
        plt.savefig(os.path.join(results_path, plt_filename))
        plt.show()

    return sched_costs, abandonment_approx

def save_frontier(alphas, sched_costs, abandonment_approx, filepath, filename, overwrite = False):
    """
    Save the efficient frontier

    Parameters
    ----------
    `alphas` : iterable
        List of alpha values the frontier was constructed over

    `sched_costs` : iterable
        List of schedule costs for each frontier instance

    `abandonment_approx` : iterable
        List of abandonment percentages for each frontier instance

    `filepath` : str
        Path to the frontier file

    `filename` : str
        Name of the frontier file

    `overwrite` : bool, optional
        Whether or not to write over any existing file, by default False
    """    
    if overwrite:
        f = open(os.path.join(filepath, filename), "w")
    else:
        f = open(os.path.join(filepath, filename), "a")
    
    lines = "alpha \t sched-cost \t abandonment\n----------------------------\n"
    for a in range(len(alphas)):
        lines += "%f \t %f \t %f\n"%(alphas[a], sched_costs[a], abandonment_approx[a])

    f.writelines(lines)

    f.close()

def read_frontier(filename, filepath):
    """
    Read a frontier file

    Parameters
    ----------
    filename : str
        Name of the frontier file

    filepath : str
        Path of the frontier file

    Returns
    -------
    `alpha` : list
        List of alpha values for the frontier

    `sched_costs` : list
        List of schedule costs for each frontier instance
    
    `abandonment` : list
        List of abandonment percentages for each frontier instance
    """    
    with open(os.path.join(filepath, filename), "r") as f:
        data = f.readlines()

    # data = lines.strip()

    alphas = []
    sched_costs = []
    abandonment = []
    for i in range(2, len(data)):
        print(data[i])
        alphas.append(float(data[i].strip().split("\t")[0]))
        sched_costs.append(float(data[i].split("\t")[1]))
        abandonment.append(float(data[i].split("\t")[2]))

    return alphas, sched_costs, abandonment

def save_sensitivity(num_runs, sched_costs, abandonment_approx, seeds, filepath, filename, overwrite = False):
    """
    Save the efficient frontier

    Parameters
    ----------
    `alphas` : iterable
        List of alpha values the frontier was constructed over

    `sched_costs` : iterable
        List of schedule costs for each frontier instance

    `abandonment_approx` : iterable
        List of abandonment percentages for each frontier instance

    `filepath` : str
        Path to the frontier file

    `filename` : str
        Name of the frontier file

    `overwrite` : bool, optional
        Whether or not to write over any existing file, by default False
    """    
    if overwrite:
        f = open(os.path.join(filepath, filename), "w")
    else:
        f = open(os.path.join(filepath, filename), "a")
    
    lines = "run \t sched-cost \t abandonment\t seed \t----------------------------\n"
    for a in range(num_runs):
        lines += "%d \t %f \t %f \t %d\n"%(a, sched_costs[a], abandonment_approx[a], seeds[a])

    f.writelines(lines)

    f.close()

def read_sensitivity(filename, filepath):
    """
    Read a frontier file

    Parameters
    ----------
    filename : str
        Name of the frontier file

    filepath : str
        Path of the frontier file

    Returns
    -------
    `alpha` : list
        List of alpha values for the frontier

    `sched_costs` : list
        List of schedule costs for each frontier instance
    
    `abandonment` : list
        List of abandonment percentages for each frontier instance
    """    
    with open(os.path.join(filepath, filename), "r") as f:
        data = f.readlines()

    # data = lines.strip()

    num_runs = 0
    sched_costs = []
    abandonment = []
    seeds = []
    for i in range(2, len(data)):
        num_runs += 1
        sched_costs.append(float(data[i].split("\t")[1]))
        abandonment.append(float(data[i].split("\t")[2]))
        seeds.append(int(data[i].split("\t")[3]))

    return num_runs, sched_costs, abandonment
    

def model_sensitivity(alpha, num_runs = 10, num_scen=500, results_dir = "Sensitivity", file_base_name="", \
    data_file="AprilData.xlsx", logging=True, time_lim = 3600, std_log = "", \
    cut_file_base_name = "", show = True):
    if file_base_name == "":
        file_base_name = "sched_base_run"

    if cut_file_base_name == "":
        cut_file_base_name = "cut_cache_run"
    
    results_path = os.path.join(DATAPATH, "..", results_dir)
    if not os.path.isdir(results_path):
        os.mkdir(results_path)

    # load data
    I, J, T, R_mean, sigma, delta, mu, server_chain = read_data(os.path.join(DATAPATH, data_file))

    # constants for SHADOW-TANDEM
    r = 100
    tau = 1
    eta = 0.01
    c = 2
    
    sched_costs = [0] * num_runs
    abandonment_percentages = np.zeros((num_runs, num_scen))

    abandonment_approx = np.zeros(num_runs)

    overall_progress = tqdm(range(num_runs))

    seeds = [randint(0, 10 ** 6) for _ in range(num_runs)]
    
    for a in overall_progress:
        overall_progress.set_description("run = %f"%a)
        filename = file_base_name + "%f.txt"%a
        cut_filename = cut_file_base_name + "%f.txt"%a

        cut_cache = None

        XV, YV, I, J, S, T, c_mip, data_time, model_time, cut_cache = base_mip(data_file, alpha, 
            num_scen = num_scen, logging=logging, time_lim = time_lim, \
            results_dir = results_dir, std_log = std_log, seed = seeds[a], filepath = results_path)
        process_cut_cache(cut_cache, cut_filename, results_dir = results_dir)

        try:
            # construct schedule and save
            sched, worker_types = construct_sched(XV, YV, I, J, S, T)
            save_sched(sched, worker_types, results_path, filename)
        except Exception:
            print(sched)
            print(worker_types)
            print(filename)

        sched_costs[a] = sum(c_mip[i, s] * XV[i, s] for (i, s) in XV)

        # begin simulation
        K = range(num_scen)
        progress = tqdm(T, total=num_scen, desc="Simulation")
        for k in K:
            Sim = Simulation(results_path, filename, I, J, T, mu, R_mean, sigma, delta, r, tau, eta, c)
            while not Sim.complete:
                p1 = Sim.period
                Sim.adv_time()
            abandonment_percentages[a, k] = Sim.abandonment_percentage
            progress.update()
            progress.set_postfix(percent_abandoned=round(Sim.abandonment_percentage * 100, 2))
        progress.close()

        abandonment_approx[a] = np.mean(abandonment_percentages[a, :])

        try:
            save_sensitivity(num_runs, sched_costs, abandonment_approx, seeds, results_path, "SensitivityResults.txt", overwrite = True)
        except Exception:
            print("num runs = ", num_runs)
            print("sched costs = ", sched_costs)
            print("abandonment approx = ", abandonment_approx)
            print("results path = ", results_path)
            raise Exception

    overall_progress.close()

    if show:
        # plot efficient frontier
        plt.scatter(sched_costs, abandonment_approx)
        plt.xlabel("Schedule Cost")
        plt.ylabel("Abandonment Percentage")
        plt.title("Efficient Frontier")
        plt_filename = "EfficientFrontier-K%d-A-%d.png"%(len(K), num_runs)
        plt.savefig(os.path.join(results_path, plt_filename))
        plt.show()

    return sched_costs, abandonment_approx


def main():
    alphas = np.linspace(0.5, 5, num=50)
    # efficient_frontier(alphas, file_base_name = 'sched_full_alpha', mode="Full")
    efficient_frontier(alphas, num_scen = 500, logging=True, time_lim = 3600, std_log = "")

    # num_runs = 10
    # alpha = 1.5
    # model_sensitivity(alpha, num_runs = num_runs, file_base_name="sched_base_run_fixed")

if __name__ == "__main__":
    main()