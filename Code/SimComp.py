import os
import numpy as np
import matplotlib.pyplot as plt
import time
import random
from gurobipy import *

from Simulation import Simulation
from Simulation2 import Simulate, sample_scens
from process_output import construct_sched, save_sched, read_sched, process_sched_levels
from data import gen_schedules, read_data, DATAPATH

r = 100
tau = 1
eta = 0.01
c = 2
data_filename = "AprilData.xlsx"
max_rostered = 100

def gen_random_sched(num_workers, I, T, S):
    # assign each worker a schedule
    scheds = {w: random.randint(0, len(S) - 1) for w in range(num_workers)}
    # assign each worker a worker type
    worker_types = {w: random.randint(0, I[-1]) for w in range(num_workers)}

    sched = np.zeros((num_workers, len(T)))

    for w in range(num_workers):
        s = S[scheds[w]]
        for ss in s:
            if ss != -1:
                sched[w, ss] = 1

    return sched, worker_types

def compare_sim(S, num_workers_ub, num_scen):
    K = range(num_scen)
    num_workers = random.randint(0, num_workers_ub)

    I, J, T, R_mean, sigma, delta, mu, server_chain = read_data(os.path.join(DATAPATH, data_filename))
    sched, worker_types = gen_random_sched(num_workers, I, T, S)

    abandonments_1 = {t: 0 for t in T}
    abandonments_2 = {t: 0 for t in T}
    abandonments_3 = {t: 0 for t in T}

    sched_abandonments_1 = {(k, t): 0 for k in K for t in T}
    sched_abandonments_2 = {(k, t): 0 for k in K for t in T}
    sched_abandonments_3 = {(k, t): 0 for k in K for t in T}

    # simulate with shadow tandem
    Gamma = np.zeros([len(J), len(T), len(K)])
    lam = np.zeros([len(J), len(T), len(K)])
    for k in K:
        R = np.random.multivariate_normal(R_mean, sigma)
        for j in J:
            for t in T:
                Gamma[j][t][k] = (R[j] ** 2 * delta[j][t])
                lam[j, t, k] = np.random.poisson(Gamma[j, t, k])

    # print("Gamma = ", Gamma)

    
    for k in K:
        sim = Simulation("", "", I, J, T, mu, R_mean, sigma, delta, 
            Gamma[:, :, k], r, tau, eta, c, sched = sched, 
            worker_types = worker_types)

        while not sim.complete:
            sim.adv_time()
        # print(sim.abandonment_profile)

        for t in T:
            sched_abandonments_1[k, t] = (sim.abandonment_profile[t]/sim.arrivals_period[t] 
                if sim.arrivals_period[t] > 0 else 0)
    
    for t in T:
        abandonments_1[t] = sum(sched_abandonments_1[k, t] for k in K)/len(K)

    # sample with simple method
    # Gamma = np.zeros([len(J), len(T), len(K2)])
    # for k in K2:
    #     R = np.random.multivariate_normal(R_mean, sigma)
    #     for j in J:
    #         for t in T:
    #             Gamma[j][t][k] = (R[j] * delta[j][t])

    Sim = sample_scens(I, J, T, K, mu, Gamma)
    level_dict = process_sched_levels(sched, worker_types, I, T)
    for k in K:
        num_abandonments, arrivals, sim_cache, serve_start, cust_start = Simulate(
            k, level_dict, Sim, {}, None, None, max_rostered, I, J, T, K, mu
        )

        for t in T:
            sched_abandonments_2[k, t] = (num_abandonments[t]/arrivals[t] if arrivals[t] > 0 else 0)

    for t in T:
        abandonments_2[t] = sum(sched_abandonments_2[k, t] for k in K)/len(K)


    m = Model("Abandonments")
    V = {(i, j, t): m.addVar(lb = 0) for i in I for j in J for t in T}
    W = {(j, t): m.addVar(lb = 0) for j in J for t in T}
    m.setObjective(quicksum(W[j, t] for j in J for t in T), GRB.MINIMIZE)
    ServeBalance = {(j, t): 
        m.addConstr(quicksum(mu[i, j] * V[i, j, t] for i in I) + W[j, t] == 0)
    for j in J for t in T}

    ServeUB = {(i, t):
        m.addConstr(quicksum(V[i, j, t] for j in J) <= level_dict[i, t])
    for i in I for t in T}

    for k in K:
        for j in J:
            for t in T:
                ServeBalance[j, t].RHS = lam[j, t, k]

        m.optimize()
        for t in T:
            sched_abandonments_3[k, t] = (sum(W[j, t].x for j in J)/sum(lam[j, t, k] for j in J)
                if sum(lam[j, t, k] for j in J) > 0 else 0)
    
    for t in T:
        abandonments_3[t] = sum(sched_abandonments_3[k, t] for k in K)/len(K)


    plt.title("Simulation Abandonments")
    plt.scatter([t for t in T], [abandonments_1[t] for t in T])
    plt.scatter([t for t in T], [abandonments_2[t] for t in T])
    plt.scatter([t for t in T], [abandonments_3[t] for t in T])
    plt.xlabel("t")
    plt.ylabel("Abandonment proportion")
    plt.legend(["SHADOW-TANDEM", "Simple Poisson", "SIP"])

    plt.show()

    return abandonments_1, abandonments_2, abandonments_3, sched
            

def main():
    I = range(6)
    T = range(34)
    SF, SP, a = gen_schedules(I, T)
    S = SF + SP
    num_workers_ub = 30
    num_scen = 1000

    abandonments_1, abandonments_2, abandonments_3, sched = compare_sim(S, num_workers_ub, num_scen)

    print("Ab 1: ", abandonments_1)
    print("Ab 2: ", abandonments_2)
    print("Ab 3: ", abandonments_3)
    print("Sched: ", sched)

    SE1 = sum((abandonments_1[t] - abandonments_2[t]) ** 2 for t in T)
    SE2 = sum((abandonments_1[t] - abandonments_3[t]) ** 2 for t in T)

    print("SE1: ", SE1)
    print("SE2: ", SE2)

    # print("Levels:")
    # for t in T:
    #     print("t = ", t)
    #     print(sum(sched[:, t]))

if __name__ == "__main__":
    main()   
